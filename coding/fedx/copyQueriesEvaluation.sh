#!/bin/bash
declare -a queries=("cd1" "cd2" "cd3" "cd4" "cd5" "cd6" "cd7" "ls1" "ls2" "ls3" "ls4" "ls5" "ls6" "ls7" "ld1" "ld2" "ld3" "ld4" "ld5" "ld6" "ld7" "ld8" "ld9" "ld10" "ld11")

for q in ${queries[@]}; do
	sed -i "43s/.*/String\tFedSummaries=\"summaries\/HibiscusOriginalSummaries.ttl\";/g" $(pwd)/src/org/aksw/simba/fedsum/startup/QueryEvaluation.java
	sed -i "44s/.*/\/\/String\tFedSummaries=\"summaries\/HibiscusSummaries.ttl\";/g" $(pwd)/src/org/aksw/simba/fedsum/startup/QueryEvaluation.java
	
	sed -i "390s/.*/\tDNFgrps=BGPGroupGenerator.generateBgpGroups($q);/g" $(pwd)/src/org/aksw/simba/fedsum/startup/QueryEvaluation.java
	sed -i "393s/.*/\tTupleQuery\ttplQuery=con.prepareTupleQuery(QueryLanguage.SPARQL,$q);/g" $(pwd)/src/org/aksw/simba/fedsum/startup/QueryEvaluation.java

	$(cp $(pwd)/src/org/aksw/simba/fedsum/startup/QueryEvaluation.java $(pwd)/src/org/aksw/simba/fedsum/startup/QueryEvaluation$q.java)
done
