#!/bin/bash
nbIter=101
count=1
totalMs=0

while [ $count -lt $nbIter ]; do
	let totalMs=$totalMs+$(java -Xmx1024m -cp bin:lib/* org.aksw.simba.fedsum.startup.QueryEvaluation | awk '/^Source/{print $4}')
	echo "Run number $count, total SS time $totalMs"
	let count=$count+1
done

let final=$totalMs/$nbIter
echo "Mean time for source selection : $final"
	
