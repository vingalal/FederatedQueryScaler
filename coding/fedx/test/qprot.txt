SELECT DISTINCT * {
?o ?p2 ?genome.
FILTER(CONTAINS(str(?genome), "GRCh38.p10"))
}
