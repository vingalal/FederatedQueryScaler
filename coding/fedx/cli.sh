#!/bin/bash
#java -Xmx4096m -cp bin:lib/* com.fluidops.fedx.CLI $*



resources="/home/vijay/git/FederatedQueryScaler/coding/resources/"
resFile="/home/vijay/git/FederatedQueryScaler/coding/fedx-grafeque/results/results.txt"

# output formats: STDOUT, XML, JSON
java -Xmx4096m -cp bin:lib/* com.fluidops.fedx.CLI -d $resources"endpoints.ttl" -f "XML" @q $resources"benchmarking/fedBench/queries/all/cd7"
# #java -Xmx4096m -cp bin:lib/* com.fluidops.fedx.CLI -d $resources"endpoints.ttl" -f "STDOUT" @q $resources"queries/q3.txt"
# #java -Xmx4096m -cp bin:lib/* com.fluidops.fedx.CLI -d $resources"endpoints.ttl" -f "XML" @q $resources"benchmarking/largeRdfBench/queries/all/ch8"


<<COMMENT


declare -a queries=("cd1" "cd2" "cd3" "cd4" "cd5" "cd6" "cd7" "ld1" "ld2" "ld3" "ld4" "ld5" "ld6" "ld7" "ld8" "ld9" "ld10" "ld11" "ls1" "ls2" "ls3" "ls4" "ls5" "ls6" "ls7")
touch $resFile
for qry in "${queries[@]}"
do
  # printf $qry"\n" >> $resFile
  printf $qry", "
  java -Xmx4096m -cp bin:lib/* com.fluidops.fedx.CLI -d $resources"endpoints.ttl" -f "XML" @q $resources"benchmarking/fedBench/queries/all/"$qry >> $resFile
  # printf "\n" >> $resFile
done
printf "\n"


COMMENT
