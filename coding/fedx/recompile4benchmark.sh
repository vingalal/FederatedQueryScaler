#!/bin/bash
nbIter=51 #+1
count=0
declare -a queries=("ls1" "ls2" "ls3" "ls4" "ls5" "ls6" "ls7") 
for i in ${queries[@]}; do
	totalMs=0
	sed -i "390s/.*/\tDNFgrps=BGPGroupGenerator.generateBgpGroups($i);/g" src/org/aksw/simba/fedsum/startup/QueryEvaluation.java
	sed -i "393s/.*/\tTupleQuery\ttplQuery=con.prepareTupleQuery(QueryLanguage.SPARQL,$i);/g" src/org/aksw/simba/fedsum/startup/QueryEvaluation.java

#	$(cat temp > src/org/aksw/simba/fedsum/startup/QueryEvaluation.java)
	echo "Benchmarking $i..."
	rm benchmarks/$i.txt
	while [ $count -lt $nbIter ];do
		let totalMs=$totalMs+$(java -Xmx2048m -cp bin:lib/* org.aksw.simba.fedsum.startup.QueryEvaluation | awk '/^Source/{print $4}')
		echo "Run $count elapsed time $totalMs" >> benchmarks/$i.txt
		let count=$count+1
		sleep 5
	done
	let final=$totalMs/$nbIter
	echo "$i performance : $final" >> benchmarks/$i.txt
	count=0

done
