* frequency and kNeighbours information for KEGG, Drugbank, and NYtimes are valid ones; rest are dummy


pg-4: NYtimes, LinkedMDB, KEGG, DrugBank
pg-5: NYtimes, LinkedMDB, KEGG, DrugBank, DBpedia
pg-7: NYtimes, LinkedMDB, KEGG, DrugBank, ChEBI, Jamendo, SWDogFood
pg-9: NYtimes, LinkedMDB, KEGG, DrugBank, ChEBI, Jamendo, SWDogFood, DBPedia, Affymetrix (105 mins [20 mins for individual; 85 minutes for pg computation])


pg-10: NYtimes, LinkedMDB, KEGG, DrugBank, ChEBI, Jamendo, SWDogFood, DBPedia, Affymetrix, GeoNames
	Settings:
		Java memory 15G
		Time: 2 hours (30 minutes for buidling uriRepository for all endoints, and 90 minutes for building predicateGraph )
				
