/*
 * @author Vijay Ingalalli
 */

package fr.inria.dyliss.grafeque.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
//
import java.util.HashMap;
import java.util.HashSet;
// import java.util.LinkedHashSet;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.BitSet;

import org.openrdf.query.algebra.helpers.StatementPatternCollector;
import org.openrdf.query.algebra.ArbitraryLengthPath;

import com.fluidops.fedx.algebra.StatementTupleExpr;
//

import org.apache.log4j.Logger;
import org.openrdf.query.algebra.QueryModelNode;
import org.openrdf.query.algebra.Service;
import org.openrdf.query.algebra.StatementPattern;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.helpers.QueryModelVisitorBase;

import com.fluidops.fedx.algebra.EmptyNJoin;
import com.fluidops.fedx.algebra.EmptyResult;
import com.fluidops.fedx.algebra.ExclusiveGroup;
import com.fluidops.fedx.algebra.ExclusiveStatement;
import com.fluidops.fedx.algebra.NJoin;
import com.fluidops.fedx.algebra.NUnion;
import com.fluidops.fedx.algebra.TrueStatementPattern;
import com.fluidops.fedx.exception.OptimizationException;
import com.fluidops.fedx.structures.QueryInfo;
import com.fluidops.fedx.util.QueryStringUtil;

import com.fluidops.fedx.algebra.FedXStatementPattern;
import org.openrdf.model.Value;
import com.fluidops.fedx.algebra.StatementSource;

import com.fluidops.fedx.optimizer.GenericInfoOptimizer;
import com.fluidops.fedx.algebra.StatementSourcePattern;

import com.fluidops.fedx.optimizer.SourceSelection;
import fr.inria.dyliss.grafeque.util.GraphProperty;
import fr.inria.dyliss.grafeque.util.NodePair;
import fr.inria.dyliss.grafeque.util.HashPair;
import fr.inria.dyliss.grafeque.util.Pair;
import fr.inria.dyliss.grafeque.graph.GraphQueryManager;
import fr.inria.dyliss.grafeque.summary.SummaryManager;
import fr.inria.dyliss.grafeque.summary.SummaryReader;

class ListKey<T> { // Using ArrayList<T> (a mutable object) as a key for HashMap.
    private ArrayList<T> list;
    @SuppressWarnings("unchecked")
    public ListKey(ArrayList<T> list) {
      this.list = (ArrayList<T>) list.clone();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        for (int i = 0; i < this.list.size(); i++) {
            T item = this.list.get(i);
            result = prime * result + ((item == null) ? 0 : item.hashCode());
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return this.list.equals(obj);
    }
}


class DfsStack2D<T> {
//	private int maxRow;
	private ArrayList<ArrayList<T>> stackArray;
//	private int row;
	private int[] pointer;

	public DfsStack2D(int stackSize) { // parameterized constructor
		stackArray = new ArrayList<ArrayList<T>>(stackSize);
		pointer = new int[stackSize];
		ArrayList<T> emptyElement = new ArrayList<T>();
		for(int i = 0 ; i < stackSize; ++i){
			stackArray.add(emptyElement); // initialize with empty ArrayList<T>
			pointer[i] = -1;
		}
	}
	public void push(ArrayList<T> stackElement, int r) { // An array is pushed onto stack
		stackArray.set(r, stackElement);
		pointer[r] = stackElement.size()-1;
	}
	public T pop(int r) { // An element from 'r'th row from stackArray is popped
//		System.out.println("pop-depth: " + r + " : " + stackArray.get(r).size() + "::" + column[r]);
	  return stackArray.get(r).get(pointer[r]--);
	}
	public T peek(int r) {
	  return stackArray.get(r).get(pointer[r]);
	}
	public boolean isEmpty(int r) {
		return (pointer[r] == -1);
	}
	// Functions for debugging
	public int getSize(int r) {
	  return stackArray.get(r).size();
	}
	public T getEdges(int r, int i) {
	  return stackArray.get(r).get(i);
	}
}

public class BgpQueryPlanner extends QueryModelVisitorBase<OptimizationException>{

	public static QueryInfo queryInfo;
  public static List<TupleExpr> unionTupleExpr = new ArrayList<>();

	protected final List<FedXStatementPattern> queryTuples = new ArrayList<FedXStatementPattern>();
	protected final ArrayList<ArrayList<String>> tripleStrings = new ArrayList<>();
	protected final ArrayList<Map<String, ArrayList<Integer>>> triplePermutations = new ArrayList<>(); // <endpoint, tripleSet>

	protected final HashMap<String, HashMap<ListKey, ArrayList<Integer>>> bgpPermMap = new HashMap<>(); // <endpoint, <<BGP, Permutation_ids>>>
	protected final ArrayList<Pair<String, ArrayList<Integer>>> invalidBGPs = new ArrayList<>(); // <endpoint, BGP> of a non-existing BGP in an endpoint.
  protected final Map<String, List<FedXStatementPattern>> epTriples = new HashMap<>(); // <a unique endpoint and the corresponding triples sent to the endpoint>; note that all the triples may not necessarily form the BGP to be sent to the endpoint as they have to form a connected graph.
  protected final ArrayList<ArrayList<String>> epPermutations = new ArrayList<>();
	protected final Set<Integer> prunablePerms = new HashSet<>();
	protected final GraphProperty queryGraph = new GraphProperty();
	protected final QueryMapping sparqlMap = new QueryMapping();
	protected final Map<Integer, Set<Integer>> nodeTripleMap = new HashMap<>(); // a set of triples for each query node.
  public static ArrayList<Set<Integer>> joinVars = new ArrayList<>(); // ArrayList<triple_ids> query varibles that are involved in join operations both globally and locally
  public static ArrayList<ArrayList<Pair<String, ArrayList<Integer>>>> prunedBgpPermutations = new ArrayList<>();
  public static Set<Integer> literalTriples = new HashSet<>();

	public BgpQueryPlanner(QueryInfo queryInfo) {
		super();
		this.queryInfo = queryInfo;
	}


	public void getQuery(TupleExpr query){
		query.visit(this);
	}


	@Override
	public void meet(Service tupleExpr) {
		// stop traversal
	}


	@Override
	public void meetOther(QueryModelNode node) {
    if(node instanceof NUnion)
      meetNUnion((NUnion) node);
    else if (node instanceof NJoin)
			meetNJoin((NJoin) node);
    else if(node instanceof ExclusiveStatement)
      unionTupleExpr.add((ExclusiveStatement) node);
    else if(node instanceof StatementSourcePattern)
      unionTupleExpr.add((StatementSourcePattern) node);
    else
			super.meetOther(node); // gets out of DFS
	}

  protected void meetNUnion(NUnion node){
    LinkedList<TupleExpr> tupleCopy = new LinkedList<TupleExpr>(node.getArgs());
    for(TupleExpr currentNode : tupleCopy)
      if(currentNode instanceof NJoin)
        meetNJoin((NJoin) currentNode);
      else // it is an instance of ExclusiveStatement or ExclusiveGroup
        unionTupleExpr.add(currentNode);
  }

	protected void meetNJoin(NJoin node) {
		LinkedList<TupleExpr> tupleCopy = new LinkedList<TupleExpr>(node.getArgs());

    if(anyEmptyStatementPattern(tupleCopy))
      return; // terminate any processing as no solutions exist.

    parseFedxQuery(tupleCopy); // Parse FedX query to be used for Grafeque
		getQueryTriples(tripleStrings, queryTuples); // Collect tripleStrings.
		GraphQueryManager.createSparqlGraph(tripleStrings, queryGraph, sparqlMap); // Create SPARQL query graph.
		ArrayList<ArrayList<Pair<String, ArrayList<Integer>>>> bgpPermutations = getBgpPermutations(); // Get all possible BGP permutations
		getGrafequeQueryPlan(bgpPermutations); // generate grafeque query plan, which is supplied to FedX query executor
	}

  protected boolean anyEmptyStatementPattern(LinkedList<TupleExpr> tuples){
    for(TupleExpr te : tuples)
      if(te instanceof EmptyResult)
        return true;
    return false;
  }

  private void parseFedxQuery(LinkedList<TupleExpr> tupleCopy){
    while (!tupleCopy.isEmpty()) { // traverse all the elements
			TupleExpr t = tupleCopy.removeFirst();
			FedXStatementPattern current = (FedXStatementPattern)t;
			queryTuples.add(current);
			ArrayList<String> endpoints = new ArrayList<>();
			for(StatementSource ep : current.getStatementSources()){
				String eps = ep.getEndpointID();
				endpoints.add(eps);
				if(epTriples.containsKey(eps)){
					List<FedXStatementPattern> triples = epTriples.get(eps);
					triples.add(current);
					epTriples.put(eps, triples);
				}
				else{
					List<FedXStatementPattern> triples = new ArrayList<>();
					triples.add(current);
					epTriples.put(eps, triples);
				}
			}
			epPermutations.add(endpoints);
			// System.out.println("EPs: " + endpoints);
		}
  }

	private void getQueryTriples(ArrayList<ArrayList<String>> tripleStrings, List<FedXStatementPattern> queryTuples){
// System.out.println("queryTuples size: " + queryTuples.size());
		for(FedXStatementPattern triple : queryTuples){
// System.out.println("queryTuples: " + triple);

			ArrayList<String> tripleString = new ArrayList<>();
			if(triple.getSubjectVar().hasValue())
				tripleString.add(triple.getSubjectVar().getValue().toString());
			else
				tripleString.add("?" + triple.getSubjectVar().getName());

      if(triple.getPredicateVar().hasValue())
			   tripleString.add(triple.getPredicateVar().getValue().toString());
      else
        tripleString.add("?" + triple.getPredicateVar().getName());

			if(triple.getObjectVar().hasValue())
				tripleString.add(triple.getObjectVar().getValue().toString());
			else
				tripleString.add("?" + triple.getObjectVar().getName());
			tripleStrings.add(tripleString);
// System.out.println("tripleString: " + tripleString);
		}
	}


	protected ArrayList<ArrayList<Pair<String, ArrayList<Integer>>>> getBgpPermutations(){
    getNodeTripleMap(); // needed for getQueryBgps.
    fetchTriplePermutations();
		// Form BGPs that can be sent to a same endpoint and store under 'bgpPermutations'
    ArrayList<ArrayList<Pair<String, ArrayList<Integer>>>> bgpPermutations = new ArrayList<>();
		for(Map<String, ArrayList<Integer>> tp :  triplePermutations){ // for each permutation
			ArrayList<Pair<String, ArrayList<Integer>>> bgpSubQ = new ArrayList<>();
			for(Map.Entry<String, ArrayList<Integer>> entry : tp.entrySet()){ // for each endpoint
				ArrayList<ArrayList<Integer>> queryBgps = new ArrayList<>();
				getQueryBgps(entry, queryBgps); // get a set of query bgps for an endpoint per permutation
				for(ArrayList<Integer> qb : queryBgps){ // if 'queryBgps' is empty, then pruning of entire permutation has happened
					Pair<String, ArrayList<Integer>> queryBgp = new Pair<String, ArrayList<Integer>>(entry.getKey(), qb);
					bgpSubQ.add(queryBgp);
				}
			}
			if(!bgpSubQ.isEmpty()) // an entire permutation is pruned as it does not contribute to final solutions.
				bgpPermutations.add(bgpSubQ);
		}
// System.out.println("All bgpPermutations:");
// for(int i = 0; i < bgpPermutations.size(); ++i){
// 	for(int j = 0; j < bgpPermutations.get(i).size(); ++j){
// 		System.out.print(bgpPermutations.get(i).get(j).first + ": " + bgpPermutations.get(i).get(j).second + "\t");
// 	}
// System.out.println();
// }

// Needed when using k-Neighbourhood summary
/*
		for(int p = 0; p < bgpPermutations.size(); ++p){
			for(Pair<String, ArrayList<Integer>> permEl : bgpPermutations.get(p)){
				if(bgpPermMap.containsKey(permEl.first)){
					if(bgpPermMap.get(permEl.first).containsKey(permEl.second))
						bgpPermMap.get(permEl.first).get(permEl.second).add(p);
					else{
						ArrayList<Integer> perms = new ArrayList<>();
						perms.add(p);
						ListKey tempList = new ListKey(permEl.second);
						bgpPermMap.get(permEl.first).put(tempList, perms);
					}
				}
				else{
					ArrayList<Integer> perms = new ArrayList<>();
					perms.add(p);
					HashMap<ListKey, ArrayList<Integer>> tempHash = new HashMap<>();
					ListKey tempList = new ListKey(permEl.second);
					tempHash.put(tempList, perms);
					bgpPermMap.put(permEl.first, tempHash);
				}
			}
		}
		System.out.println("PermBgpMap size: " + bgpPermMap.size());

*/
    return bgpPermutations;
	}

  protected void getNodeTripleMap(){
    int tripleId = 0;
    for(Map.Entry<NodePair, ArrayList<Integer>> entry : queryGraph.edges.entrySet()){
      if(nodeTripleMap.containsKey(entry.getKey().getFirst()))
        nodeTripleMap.get(entry.getKey().getFirst()).add(tripleId);
      else{
        Set<Integer> triples = new HashSet<>();
        triples.add(tripleId);
        nodeTripleMap.put(entry.getKey().getFirst(), triples);
      }
      if(nodeTripleMap.containsKey(entry.getKey().getSecond()))
        nodeTripleMap.get(entry.getKey().getSecond()).add(tripleId);
      else{
        Set<Integer> triples = new HashSet<>();
        triples.add(tripleId);
        nodeTripleMap.put(entry.getKey().getSecond(), triples);
      }
      ++tripleId;
    }
    for(Integer l : sparqlMap.literalNodes)
      literalTriples.addAll(nodeTripleMap.get(l));
// System.out.println("nodeTripleMap");
// for(Map.Entry<Integer, Set<Integer>> entry : nodeTripleMap.entrySet())
//   System.out.println(entry.getKey() + " : " + entry.getValue());
  }

  protected void fetchTriplePermutations(){ // A DFS approach to fetch all triple permutations
		int K = epPermutations.size();
		for(String epp : epPermutations.get(0)){
			ArrayList<Pair<String, Integer>> singlePermutation = new ArrayList<>(K); // <source, triple_id>
			for(int i = 0; i < K; ++i)
				singlePermutation.add(null);
			Pair<String, Integer> initPair = new Pair<>(epp, 0);
			singlePermutation.set(0, initPair);
			int depth = 1;
			DfsStack2D<String> permutationDfsStack = new DfsStack2D<String>(K-1);
			boolean fwd = true;
			while(depth != 0){
				if(depth == K){
					getDfsSolutions(singlePermutation); // Collect the permutations
					--depth;
					permutationDfsStack.pop(depth-1);
					fwd = false;
				}
				else{
					if(fwd){
						permutationDfsStack.push(epPermutations.get(depth), depth-1);
					}
					else
						permutationDfsStack.pop(depth-1);
				}
				if(!permutationDfsStack.isEmpty(depth-1)){
					Pair<String, Integer> depthPair = new Pair<>(permutationDfsStack.peek(depth-1), depth);
					singlePermutation.set(depth, depthPair);
					++depth;
					fwd = true;
				}
				else{
					--depth;
					fwd = false;
				}
			}
		}
	}

  protected void getDfsSolutions(ArrayList<Pair<String, Integer>> singlePermutation){
		Map<String, ArrayList<Integer>>  singlePermutationHash = new HashMap<>();
		for(Pair<String, Integer> sp : singlePermutation){
			if(singlePermutationHash.containsKey(sp.first)){
				ArrayList<Integer> qIds = singlePermutationHash.get(sp.first);
				qIds.add(sp.second);
				singlePermutationHash.replace(sp.first, qIds);
			}
			else{
				ArrayList<Integer> qIds = new ArrayList<>();
				qIds.add(sp.second);
				singlePermutationHash.put(sp.first, qIds);
			}
		}
		triplePermutations.add(singlePermutationHash);
// System.out.print("unique: ");
// for(Pair<String, Integer> sp : singlePermutation)
// 	System.out.print(sp.first + " : " + sp.second + "\t");
// System.out.println();
	}


	protected void getQueryBgps(Map.Entry<String, ArrayList<Integer>> subQuery, ArrayList<ArrayList<Integer>> queryBgps){ // collect the connected components of a BGP
		// Collect the connected components of a BGP
		// Get mapped query
		// Convert string BGP into integer BGP
		// Check for frontier connectivity
		// List<FedXStatementPattern> tripleSet = new ArrayList<>();
		String ep = subQuery.getKey();
// System.out.println("Subquery: " + subQuery.getKey() + " : " + subQuery.getValue());
		List<FedXStatementPattern> subQueryTriples = new ArrayList<>();
		for(Integer t : subQuery.getValue())
			subQueryTriples.add(queryTuples.get(t));

		ArrayList<ArrayList<String>> subTripleStrings = new ArrayList<>();
		getQueryTriples(subTripleStrings, subQueryTriples);

		ArrayList<ArrayList<Integer>> graphSubQuery = new ArrayList<>(); // ArrayList<S, O, P1,...Pn> : a multi subgraph query
		ArrayList<Set<Integer>> subqueryEntities = new ArrayList<>();
		// get the mapped integer values of triples

		for(ArrayList<String> triple : subTripleStrings){
			ArrayList<Integer> tripleInt = new ArrayList<>();
			Set<Integer> entities = new HashSet<>();
			entities.add(sparqlMap.nodeMap.get(triple.get(0)));
			entities.add(sparqlMap.nodeMap.get(triple.get(2)));
			subqueryEntities.add(entities);

			tripleInt.add(sparqlMap.nodeMap.get(triple.get(0)));
			tripleInt.add(sparqlMap.edgeMap.get(triple.get(1)));
			tripleInt.add(sparqlMap.nodeMap.get(triple.get(2)));
			graphSubQuery.add(tripleInt);
		}

// Get adjacent vertices:
		Map<Integer, Set<Integer>> adjListMap = new HashMap<>();
		getAdjList(graphSubQuery, adjListMap);

		Set<Integer> subQNodes = new HashSet<>();
		for(ArrayList<String> triple : subTripleStrings){
			subQNodes.add(sparqlMap.nodeMap.get(triple.get(0)));
			subQNodes.add(sparqlMap.nodeMap.get(triple.get(2)));
		}
// System.out.println("SubQNodes: " + subQNodes);

		boolean[] nodeVisited = new boolean[subQNodes.size()];
		Map<Integer, Integer> nMap = new HashMap<>();
		int valN = 0;
		for(Integer keyN : subQNodes)
			nMap.put(keyN, valN++);

		for(Integer node : subQNodes){
			List<Integer> connectedNodes = new ArrayList<>();
			if(!nodeVisited[nMap.get(node)]){
				Queue<Integer> frontier = new LinkedList<>(); // for BFS traversal
				frontier.add(node);
				connectedNodes.add(node);
				nodeVisited[nMap.get(node)] = true;
				while(!frontier.isEmpty()){
					int nextN = frontier.remove();
					for(Integer adjNode : adjListMap.get(nextN)){
						if(nMap.containsKey(adjNode)){ // pbbly not required.
							if(!nodeVisited[nMap.get(adjNode)]){
								frontier.add(adjNode);
								connectedNodes.add(adjNode);
								nodeVisited[nMap.get(adjNode)] = true;
							}
						}
					}
				}
				Set<Integer> bgpTriples = new HashSet<>();
				for(Integer nodeC : connectedNodes)
					bgpTriples.addAll(nodeTripleMap.get(nodeC));
				ArrayList<Integer> triplesList = new ArrayList<>();
				for(Integer t : subQuery.getValue()){
					if(bgpTriples.contains(t))
						triplesList.add(t);
				}
				queryBgps.add(triplesList);
			}
		}
	}

  private static void getAdjList(ArrayList<ArrayList<Integer>> graphSubQuery, Map<Integer, Set<Integer>> adjListMap){
		for(ArrayList<Integer> triple : graphSubQuery){
			if(adjListMap.containsKey(triple.get(0)))
				adjListMap.get(triple.get(0)).add(triple.get(2));
			else{
				Set<Integer> newT = new HashSet<>();
				newT.add(triple.get(2));
				adjListMap.put(triple.get(0), newT);
			}
			if(adjListMap.containsKey(triple.get(2)))
				adjListMap.get(triple.get(2)).add(triple.get(0));
			else{
				Set<Integer> newT = new HashSet<>();
				newT.add(triple.get(0));
				adjListMap.put(triple.get(2), newT);
			}
			// System.out.println("Subquery adjListTEMP: " + adjListMap.size());
	    // for(Map.Entry<Integer, Set<Integer>> entry : adjListMap.entrySet()){
			// 	System.out.println(entry.getKey() + " : " + entry.getValue());
		  // }
		}
		// System.out.println("Subquery adjList: " + adjListMap.size());
    // for(Map.Entry<Integer, Set<Integer>> entry : adjListMap.entrySet()){
		// 	System.out.println(entry.getKey() + " : " + entry.getValue());
	  // }
	}

	protected void getGrafequeQueryPlan(ArrayList<ArrayList<Pair<String, ArrayList<Integer>>>> bgpPermutations){
		/*
		* Collecting BGPs for both 'kNeighbors' summary and 'predicateGraph' summary
		* in itself is part of the query planning. The BgpQueryPlan should have
		* the BGPs formulated in such a manner to facilitate querying the
		* above 2 summaries. Make sure that the query plan is complete; the summaries
		* would reduce possible query planning combinations.
		*/
    SummaryManager summaryManager = new SummaryManager();

    // 1. Prune BGP permutations using Neighbourhood Summary Index
/*
    Map<ArrayList<Integer>, HashSet<String>> uniqueBgpPermutations = new HashMap<>();
		// Map<ArrayList<Integer>, HashSet<String>> invalidBgpPermutations = new HashMap<>();

		for(int i = 0; i < bgpPermutations.size(); ++i){
			for(Pair<String, ArrayList<Integer>> element : bgpPermutations.get(i)){
				if(uniqueBgpPermutations.containsKey(element.second))
					uniqueBgpPermutations.get(element.second).add(element.first.replace("sparql_",""));
				else{
					HashSet<String> tmp = new HashSet<>();
					tmp.add(element.first.replace("sparql_",""));
					uniqueBgpPermutations.put(element.second, tmp);
				}
			}
		}
		System.out.println("UniqueBgpSize: " + uniqueBgpPermutations.size());
		System.out.println("Summaries in BGP file: " + SummaryReader.summaries.size());
    System.out.println("Predicate Graph summary in BGP file: " + SummaryReader.predicateGraphSummary.size());


		for(Map.Entry<ArrayList<Integer>, HashSet<String>> entry : uniqueBgpPermutations.entrySet()){
			ArrayList<ArrayList<String>> uniqueBgpTriples = new ArrayList<>();
			for(Integer tripleId : entry.getKey())
				uniqueBgpTriples.add(tripleStrings.get(tripleId));
			for(String ep : entry.getValue()){ //for each end point of a BGP
				GraphProperty bgpGraph = new GraphProperty();
				GraphQueryManager.createBgpGraph(uniqueBgpTriples, SummaryReader.summaries.get(ep), bgpGraph);
				System.out.println("ENDPOINT: " + ep);
				if(!summaryManager.queryNeighSummary(1, bgpGraph, SummaryReader.summaries.get(ep).kNeighbours)){ // BGP does not exist in an ep
					Pair nBgp = new Pair(ep, entry.getKey());
					invalidBGPs.add(nBgp); // a BGP does not exist in endpoint ep.
				}
			}
		}

		for(Pair<String, ArrayList<Integer>> el : invalidBGPs){
			if(bgpPermMap.containsKey(el.first)){
				ListKey tempList = new ListKey(el.second);
				if(bgpPermMap.get(el.first).containsKey(tempList))
					prunablePerms.addAll(bgpPermMap.get(el.first).get(tempList));
			}
		}

System.out.println("Pruned Permutations using Neigh summary : " + prunablePerms);
*/

    // 2. Prune BGP permutations using Predicate Graph Summary Index
    // TODO Use pruned-bgpPermutations as in put for pruning  summary graph.

    for(Map.Entry<Integer, Set<Integer>> entry : nodeTripleMap.entrySet())
      if(entry.getValue().size() > 1) // a global variable spans across more than 1 triple
        joinVars.add(entry.getValue()); //

// System.out.println("joinVars: " + joinVars);


    // System.out.println("# bgpCombinations before pruning: " + bgpPermutations.size());
// Forcably turnoff the pruning step
    for(int i = 0; i < bgpPermutations.size(); ++i){
      if(isInvalidPermutation(bgpPermutations.get(i))){
        prunablePerms.add(i);
      }
    }

    for(int i = 0; i < bgpPermutations.size(); ++i)
      if(!prunablePerms.contains(i))
        prunedBgpPermutations.add(bgpPermutations.get(i));
    // System.out.println("# bgpCombinations after pruning: " + prunedBgpPermutations.size());

    // System.out.println("Pruned BGP combinations:");
    // System.out.println("*****");
    // for(int i = 0; i < prunedBgpPermutations.size(); ++i){
    // 	for(int j = 0; j < prunedBgpPermutations.get(i).size(); ++j){
    // 		System.out.print(prunedBgpPermutations.get(i).get(j).first + ": " + prunedBgpPermutations.get(i).get(j).second + "\t");
    // 	}
    // System.out.println();
    // }
    // System.out.println("*****");
	}

  private boolean isInvalidPermutation(ArrayList<Pair<String, ArrayList<Integer>>> bgpPermutation){
    for(int i = 0; i < bgpPermutation.size()-1; ++i){
      for(int j = i+1; j < bgpPermutation.size(); ++j){
          if(!doJoinableTriplesHaveSolutions(i, j, bgpPermutation)){ // there could be some non-joinable BGPs, that of course do not have any solutions
            return true; // prunable permutation
          }
      }
    }
    return false;
  }

  private boolean doJoinableTriplesHaveSolutions(int a , int b, ArrayList<Pair<String, ArrayList<Integer>>> bgpPermutation){
    int noJoin = 0;
    ArrayList<Integer> tripleSetA = bgpPermutation.get(a).second;
    ArrayList<Integer> tripleSetB = bgpPermutation.get(b).second;
    Pair<String, String> endpoint = new Pair<String, String>(bgpPermutation.get(a).first.replace("sparql_", ""), bgpPermutation.get(b).first.replace("sparql_", ""));
    for(int j = 0; j < tripleSetB.size(); ++j){
      for(int i = 0; i < tripleSetA.size(); ++i){
        ArrayList<Integer> triplePair = new ArrayList<>();
        triplePair.add(tripleSetA.get(i));
        triplePair.add(tripleSetB.get(j));
        for(int k = 0; k < joinVars.size(); ++k){
          if(joinVars.get(k).containsAll(triplePair)){ // global join exists for this pair of BGPs
            Pair<Integer, Integer> globalJoinTriplePair = new Pair<>(tripleSetA.get(i), tripleSetB.get(j));
              if(!isPairInPredicateGraph(globalJoinTriplePair, endpoint)){ // triplePair has a global join, and hence may / may not exist in predicateGraph
                return false; // Globally joinable triple pairs have NO solutions. Hence this combination of BGP returns no solutions;
              }
          }
          else{
            ++noJoin;
          }
        }
      }
    }
    return true;
  }

  private boolean isPairInPredicateGraph(Pair<Integer, Integer> triplePair, Pair<String, String> endpoint){
// System.out.println("predicateGraphSummary SIZE: " + SummaryReader.predicateGraphSummary.size());

    ArrayList<String> tripleA = tripleStrings.get(triplePair.first);
    ArrayList<String> tripleB = tripleStrings.get(triplePair.second);

    // If one of the predicates is a variable (?predicate), then for each entry in the PG graph, we check if we can find the substring of a pair entry.
    if(tripleA.get(1).startsWith("?"))
      return isPairWithVariableInPredicateGraph(":" + endpoint.first, tripleB.get(1) + ":" + endpoint.second);
    else if(tripleB.get(1).startsWith("?"))
      return  isPairWithVariableInPredicateGraph(":" + endpoint.second, tripleA.get(1) + ":" + endpoint.first);

    int edgeLabel;
    if(tripleA.get(0).equals(tripleB.get(0))) // subject-subject
      edgeLabel = 1;
    else if(tripleA.get(0).equals(tripleB.get(2))) // subject-object
      edgeLabel = 2;
    else if(tripleA.get(2).equals(tripleB.get(0))) // object-subject
      edgeLabel = 3;
    else // object-object
      edgeLabel = 4;

    String predPair = tripleA.get(1) + ":" + endpoint.first + " " + tripleB.get(1) + ":" + endpoint.second;
    String predPairRev = tripleB.get(1) + ":" + endpoint.second + " " + tripleA.get(1) + ":" + endpoint.first;

// System.out.println("predPair: " + predPair);

    if(SummaryReader.predicateGraphSummary.containsKey(predPair)){
      if(!SummaryReader.predicateGraphSummary.get(predPair).contains(edgeLabel))
        return false;  // edge lable does not match.
    }
    else if(SummaryReader.predicateGraphSummary.containsKey(predPairRev)){
      if(edgeLabel == 2){
        if(!SummaryReader.predicateGraphSummary.get(predPairRev).contains(3))
          return false;  // edge lable does not match.
      }
      else if(edgeLabel == 3){
        if(!SummaryReader.predicateGraphSummary.get(predPairRev).contains(2))
          return false;  // edge lable does not match.
      }
      else
        if(!SummaryReader.predicateGraphSummary.get(predPairRev).contains(edgeLabel)) // when edgeLabel is either '1' or '4' reverse edge doesnot matter, as the predicate position in s-s or o-o doesnt change.
          return false;  // edge lable does not match.
    }
    else{
      return false; // neither the 'predPair' nor 'predPairRev' exist
    }
    return true; // global join pair exists in the predicateGraph Summary.
  }

  private boolean isPairWithVariableInPredicateGraph(String predicateVar, String predicate) {
    for (String str : SummaryReader.predicateGraphSummary.keySet())
        if (str.indexOf(predicateVar) >= 0 && str.indexOf(predicate) >= 0)
            return true;
    return false;
    // Possible one liner code
    // return SummaryReader.predicateGraphSummary.keySet().stream().anyMatch(str->str.indexOf(predicateVar) >= 0 && str->str.indexOf(predicate) >= 0 );
  }

}
