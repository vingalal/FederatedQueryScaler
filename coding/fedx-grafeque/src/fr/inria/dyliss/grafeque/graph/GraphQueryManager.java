/*
 * @author Vijay Ingalalli
 */


package fr.inria.dyliss.grafeque.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.BitSet;

import fr.inria.dyliss.grafeque.util.GraphProperty;
import fr.inria.dyliss.grafeque.util.NodePair;
import fr.inria.dyliss.grafeque.util.RdfSummary;


class QueryMapping{ // Accessed only under *.graph package.
	public static Map<String, Integer> nodeMap = new HashMap<String, Integer>();
	public static Map<String, Integer> edgeMap = new HashMap<String, Integer>();
	public static HashSet<Integer> literalNodes = new HashSet<>();
}


public class GraphQueryManager {

	static Map<String, String> predicatesFull = new HashMap<String, String>(); // <truncatedPredicate, extendedPredicate>
  private static String removeLastChar(String str) {
    return str.substring(0, str.length() - 1);
  }

	private static void getAdjList(GraphProperty graphData, int nNodes){
		graphData.adjList.ensureCapacity(nNodes);
  	for(int i  = 0; i < nNodes; ++i)
    	graphData.adjList.add(new HashSet<Integer>());

    for(Map.Entry<NodePair, ArrayList<Integer>> entry : graphData.edges.entrySet()){
	  	int nSrc = entry.getKey().getFirst();
			int nObj = entry.getKey().getSecond();
			graphData.adjList.get(nSrc).add(nObj);
			graphData.adjList.get(nObj).add(nSrc);
	  }
		// System.out.println("Adjacency list: " + graphData.adjList);
	}


	protected static void createSparqlGraph(ArrayList<ArrayList<String>> tripleStrings, GraphProperty sparqlGraph, QueryMapping sparqlMap){
		int node = 0, edge = 0;

		for(int i = 0; i < tripleStrings.size(); ++i){
// System.out.println(tripleStrings.get(i));

			int nodeS, nodeO, edgeP;
			if(!sparqlMap.nodeMap.containsKey(tripleStrings.get(i).get(0))){ // subject
				sparqlMap.nodeMap.put(tripleStrings.get(i).get(0), node);
				nodeS = node;
				++node;
			}
			else
				nodeS = sparqlMap.nodeMap.get(tripleStrings.get(i).get(0));

			if(!tripleStrings.get(i).get(0).startsWith("?"))
			// if(tripleStrings.get(i).get(0).startsWith("'") || tripleStrings.get(i).get(0).startsWith("\""))
				sparqlMap.literalNodes.add(nodeS);

			if(!sparqlMap.nodeMap.containsKey(tripleStrings.get(i).get(2))){ // object
				sparqlMap.nodeMap.put(tripleStrings.get(i).get(2), node);
				nodeO = node;
				++node;
			}
			else
				nodeO = sparqlMap.nodeMap.get(tripleStrings.get(i).get(2));

			if(!tripleStrings.get(i).get(2).startsWith("?"))
			// if(tripleStrings.get(i).get(0).startsWith("'") || tripleStrings.get(i).get(0).startsWith("\""))
				sparqlMap.literalNodes.add(nodeO);

			if(!sparqlMap.edgeMap.containsKey(tripleStrings.get(i).get(1))){ // predicate
				sparqlMap.edgeMap.put(tripleStrings.get(i).get(1), edge);
				edgeP = edge;
				++edge;
			}
			else
				edgeP = sparqlMap.edgeMap.get(tripleStrings.get(i).get(1));

			NodePair qNodes = new NodePair(nodeS, nodeO);
			ArrayList<Integer> qEdges = new ArrayList<Integer>();

			if(sparqlGraph.edges.containsKey(qNodes)){ // a multiedge exists
				ArrayList<Integer> qEdgesUpdated = sparqlGraph.edges.get(qNodes);
				qEdgesUpdated.add(edgeP);
				sparqlGraph.edges.replace(qNodes, qEdgesUpdated);
			}
			else{
				qEdges.add(edgeP);
				sparqlGraph.edges.put(qNodes, qEdges);
			}
		}
// for(Map.Entry<String, Integer> entry : sparqlMap.nodeMap.entrySet())
// System.out.println(entry.getKey() + ", " + entry.getValue());
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : sparqlGraph.edges.entrySet()){
			if(entry.getValue().size() == 1)
				sparqlGraph.uniqueEdges.add(entry.getValue().get(0));
			else
				sparqlGraph.uniqueMultiedges.add(entry.getValue());
		}
		getAdjList(sparqlGraph, sparqlMap.nodeMap.size());
	}




	protected static void createBgpGraph(ArrayList<ArrayList<String>> tripleStrings, RdfSummary rdfSummary, GraphProperty bgpGraph){
		Map<String, Integer> nodeMap = new HashMap<String, Integer>();
		int node = 0, edge = 0;

// System.out.println("#Triples in BGP: " + tripleStrings.size());
		for(int i = 0; i < tripleStrings.size(); ++i){
			int nS, nO, eP;
			if(!nodeMap.containsKey(tripleStrings.get(i).get(0))){ // subject
				nodeMap.put(tripleStrings.get(i).get(0), node);
				nS = node;
				++node;
			}
			else
				nS = nodeMap.get(tripleStrings.get(i).get(0));

			if(!nodeMap.containsKey(tripleStrings.get(i).get(2))){ // object
				nodeMap.put(tripleStrings.get(i).get(2), node);
				nO = node;
				++node;
			}
			else
				nO = nodeMap.get(tripleStrings.get(i).get(2));

// System.out.println(rdfSummary.predicateMap.size() + " : " + tripleStrings.get(i).get(1));
//
// if(rdfSummary.predicateMap.containsKey("<" + tripleStrings.get(i).get(1) + ">"))
// 	System.out.println("YES");
// else
// System.out.println("NOO: " + "<" + tripleStrings.get(i).get(1) + ">");

			eP = rdfSummary.predicateMap.get("<" + tripleStrings.get(i).get(1) + ">");

// System.out.println(nS + " " + nO + " " + eP);
			NodePair qNodes = new NodePair(nS, nO);
			ArrayList<Integer> qEdges = new ArrayList<Integer>();

			if(bgpGraph.edges.containsKey(qNodes)){ // a multiedge exists
				ArrayList<Integer> qEdgesUpdated = bgpGraph.edges.get(qNodes);
				qEdgesUpdated.add(eP);
				bgpGraph.edges.replace(qNodes, qEdgesUpdated);
			}
			else{
				qEdges.add(eP);
				bgpGraph.edges.put(qNodes, qEdges);
			}
		}
// for(Map.Entry<String, Integer> entry : sparqlMap.nodeMap.entrySet())
// System.out.println(entry.getKey() + ", " + entry.getValue());
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : bgpGraph.edges.entrySet()){
			if(entry.getValue().size() == 1)
				bgpGraph.uniqueEdges.add(entry.getValue().get(0));
			else
				bgpGraph.uniqueMultiedges.add(entry.getValue());
		}
		getAdjList(bgpGraph, nodeMap.size());
	}
}
