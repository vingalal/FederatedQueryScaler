
package fr.inria.dyliss.grafeque.evaluation;

import java.util.List;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;

import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.StatementPattern;
import com.fluidops.fedx.algebra.StatementSource;
import com.fluidops.fedx.algebra.ExclusiveGroup;
import com.fluidops.fedx.algebra.ExclusiveStatement;
import com.fluidops.fedx.optimizer.SourceSelection;
import com.fluidops.fedx.algebra.FedXStatementPattern;

import fr.inria.dyliss.grafeque.planoptimizer.Optimizer;
import fr.inria.dyliss.grafeque.graph.BgpQueryPlanner;
import fr.inria.dyliss.grafeque.util.Pair;

class UniqueBGP{
 public ArrayList<Integer> bgp;
 public ArrayList<Integer> permList; // list of all the permutations where bgp exists.
 public String ep;

 public UniqueBGP(ArrayList<Integer> bgp, ArrayList<Integer> permList, String ep){
   this.bgp = bgp;
   this.permList = permList;
   this.ep = ep;
 }
}

public class GrafequeEvaluator{
  public void getUniqueBgpsForEvaluation(List<TupleExpr> uniqueBgpsForEvaluation, ArrayList<Integer> initialBGPs, ArrayList<ArrayList<Integer>> queryPlan){

    // 1. Collect Unique BGPs with corresponding endpoints
    HashMap<ArrayList<Integer>, HashMap<String, ArrayList<Integer>>> uniqueBgpMapping = new HashMap<>();
    for(int i = 0; i < BgpQueryPlanner.prunedBgpPermutations.size(); ++i){
      for(Pair<String, ArrayList<Integer>> element : BgpQueryPlanner.prunedBgpPermutations.get(i)){
        if(uniqueBgpMapping.containsKey(element.second)){ // bgp repeats
          if(uniqueBgpMapping.get(element.second).containsKey(element.first)){ //ep repeats
            uniqueBgpMapping.get(element.second).get(element.first).add(i);
          }
          else{ // ep does not repeat
            ArrayList<Integer> permList = new ArrayList<>();
            permList.add(i);
            uniqueBgpMapping.get(element.second).put(element.first, permList); // new ep and permList
          }
        }
        else{ // new bgp
          HashMap<String, ArrayList<Integer>> temp = new HashMap<>();
          ArrayList<Integer> permList = new ArrayList<>();
          permList.add(i);
          temp.put(element.first, permList); // new ep and permList
          uniqueBgpMapping.put(element.second, temp);
        }
      }
    }

    // 1.a. Maintain "uniqueBGPs" for creating ExclusiveGroup for BGPs
    ArrayList<UniqueBGP> uniqueBGPs = new ArrayList<>();
    for(Map.Entry<ArrayList<Integer>, HashMap<String, ArrayList<Integer>>> entry_1 : uniqueBgpMapping.entrySet()){
      for(Map.Entry<String, ArrayList<Integer>> entry_2 : entry_1.getValue().entrySet()){
        UniqueBGP uniqueBGP = new UniqueBGP(entry_1.getKey(), entry_2.getValue(), entry_2.getKey());
        uniqueBGPs.add(uniqueBGP);
      }
    }

    ArrayList<Integer>	initBGPs = getInitialBgpsForEvaluation(uniqueBGPs, BgpQueryPlanner.prunedBgpPermutations.size());
    for(Integer iB : initBGPs)
      initialBGPs.add(iB);

// System.out.println("initialBGPs: " + initialBGPs);
// for(UniqueBGP uniqueBGP :uniqueBGPs){
//   System.out.println(uniqueBGP.bgp + " : " + uniqueBGP.ep);
// }

    HashMap<ArrayList<Integer>, HashMap<String, Integer>> uniqueMappings = new HashMap<>();
    for(int i = 0; i < uniqueBGPs.size(); ++i){
      if(uniqueMappings.containsKey(uniqueBGPs.get(i).bgp)){
        uniqueMappings.get(uniqueBGPs.get(i).bgp).put(uniqueBGPs.get(i).ep, i);
      }
      else{
        HashMap<String, Integer> tmp = new HashMap<>();
        tmp.put(uniqueBGPs.get(i).ep, i);
        uniqueMappings.put(uniqueBGPs.get(i).bgp, tmp);
      }
    }

    for(ArrayList<Pair<String, ArrayList<Integer>>> perm : BgpQueryPlanner.prunedBgpPermutations){
      ArrayList<Integer> tempPlan = new ArrayList<>();
      for(Pair<String, ArrayList<Integer>> pBgp : perm){
        tempPlan.add(uniqueMappings.get(pBgp.second).get(pBgp.first));
      }
      queryPlan.add(getFinalPlan(tempPlan, initialBGPs, uniqueBGPs));
      // queryPlan.add(tempPlan);
      // System.out.println("tempPlan: " + tempPlan);
      // ArrayList<Integer> finalPlan = getFinalPlan(tempPlan, initialBGPs, uniqueBGPs);
      // System.out.println("finalPlan: " + finalPlan);
    }

    // 2. Create ExclusiveStatement for each StatementPattern, irrespective of #endpoints (needed for grafeque query plan)

    List<List<ExclusiveStatement>> exclusiveStatements = new ArrayList<>();
    int s = 0;
    for(StatementPattern stmt : Optimizer.statementPatterns){
      if(isUnionTuple(stmt, BgpQueryPlanner.unionTupleExpr)){
        ++s;
        continue;
      }
      List<ExclusiveStatement> excls = new ArrayList<>();
      for(int t = 0; t < SourceSelection.stmtSources.get(s).size(); ++t){
        ExclusiveStatement excl = new ExclusiveStatement(stmt, SourceSelection.stmtSources.get(s).get(t), BgpQueryPlanner.queryInfo);
        excls.add(excl);
      }
      exclusiveStatements.add(excls);
      ++s;
    }

    // 3. ExclusiveGroup for BGPs of grafeque query plan.
    for(int m = 0; m < uniqueBGPs.size(); ++m){
      List<ExclusiveStatement> exclList = new ArrayList<>();
      String bgpEp =  uniqueBGPs.get(m).ep;
      StatementSource bgpSource = null;
      for(Integer i : uniqueBGPs.get(m).bgp){
        for(ExclusiveStatement current : exclusiveStatements.get(i)){
          if(bgpEp.equals(current.getStatementSources().get(0).getEndpointID())) { // "getStatementSources.get(0)", because each 'ExclusiveStatement' has only one endpoint
            exclList.add(current);
            bgpSource = current.getStatementSources().get(0);
            break;
          }
        }
      }
      TupleExpr bgpTuple =  new ExclusiveGroup(exclList, bgpSource, BgpQueryPlanner.queryInfo); //TODO replace "SourceSelection.stmtSources.get(0).get(0)" with bgpSource
      uniqueBgpsForEvaluation.add(bgpTuple);
      // System.out.println("BGP TO be evaluated " + bgpTuple);
    }
  }

  private ArrayList<Integer> getInitialBgpsForEvaluation(ArrayList<UniqueBGP> uniqueBGPs, int noOfPerms){
    ArrayList<Integer> initBGPs = new ArrayList<>();

    // 1. If SPARQL query has literals, collect the BGPs that have literals

    Set<Integer> literalBGPs = new HashSet<>();
    for(Integer l : BgpQueryPlanner.literalTriples){
      for(int u = 0; u < uniqueBGPs.size(); ++u){
        if(uniqueBGPs.get(u).bgp.contains(l))
          literalBGPs.add(u);
      }
    }

// System.out.println("literalBGPs: " + literalBGPs);
    // 2. Check if literal BGPs are enough to cover all the permutations; if so, return initBGPs.
    HashSet<Integer> pCover = new HashSet<>();
    for(Integer lb : literalBGPs){
      pCover.addAll(uniqueBGPs.get(lb).permList);
      initBGPs.add(lb);
      if(pCover.size() == noOfPerms){
        return initBGPs;
      }
    }

    // 3. If literal BGPs are not enough to cover all the BGPs launch the following code
    int maxSize = uniqueBGPs.size();
    int[] intArray = new int[uniqueBGPs.size()];
    for(int i = 0; i < uniqueBGPs.size(); ++i)
      intArray[i] = i;

    // Run a loop for printing all 2^n subsets one by one
    for (int i = 0; i < (1<<uniqueBGPs.size()); i++){
        ArrayList<Integer> initialBGPsTemp = new ArrayList<>();
        // Print current subset
        for (int j = 0; j < uniqueBGPs.size(); j++)
          if ((i & (1 << j)) > 0)
            initialBGPsTemp.add(intArray[j]);
            // (1<<j) is a number with jth bit 1; so when we 'and' them with the subset number we get which numbers are present in the subset and which  are not

        if(initialBGPsTemp.size() > 1 && initialBGPsTemp.size() < maxSize){
          HashSet<Integer> permCover = new HashSet<>();
          for(Integer is : initialBGPsTemp){
            permCover.addAll(uniqueBGPs.get(is).permList);
          }
          if(permCover.size() == noOfPerms){ // all permutations are covered
            if(initialBGPsTemp.size() == 2){
              return initialBGPsTemp;
            }
            else{
              initBGPs = initialBGPsTemp;
              maxSize = initialBGPsTemp.size();
            }
          }
        }
    }
    return initBGPs;
  }

  /*
  	* This query plan chooses minimal bumber of BGPs that are common to all permutations.
  	*	Only initial BGPs are evaluated independently for Solutions
  	* Rest of the BGPs in a permutations are ordered in a way that they have join operations with the previous BGP
  	* Thus, except initial BGP, rest of BGPs in a permutation are computed using results from previous BGPs
  */
	private ArrayList<Integer> getFinalPlan(ArrayList<Integer> bgps, ArrayList<Integer> initBgps, ArrayList<UniqueBGP> uniqueBGPs){
		ArrayList<Integer> finalPlan = new ArrayList<>();
		int initBGP = 0;
		for(Integer b : initBgps)
			if(bgps.contains(b))
				initBGP = b;
		bgps.remove(Integer.valueOf(initBGP));
		finalPlan.add(initBGP);
		int head = initBGP;
    @SuppressWarnings("unchecked")
		ArrayList<Integer> headTriples = (ArrayList<Integer>)uniqueBGPs.get(initBGP).bgp.clone();
		while(bgps.size() != 0){
			Iterator<Integer> it = bgps.iterator();
			while (it.hasNext()) {
				Integer nextBGP = it.next();
// System.out.println("nextBGP: " + nextBGP);
				ArrayList<Integer> tailTriples = uniqueBGPs.get(nextBGP).bgp;
				if(doBgpsHaveJoin(headTriples, tailTriples)){
					finalPlan.add(nextBGP);
					// headTriples = tailTriples;
          headTriples.addAll(tailTriples);
					it.remove();
				}
			}
// System.out.println("tempPlan size: " + bgps.size());
		}
		return finalPlan;
	}

  private boolean doBgpsHaveJoin(ArrayList<Integer> hT, ArrayList<Integer> tT){
		for(Integer h : hT){
			for(Integer t : tT){
				for(Set<Integer> jV : BgpQueryPlanner.joinVars){
					if(jV.contains(h) && jV.contains(t)){
// System.out.println("JOIN: " + h + " : " + t);
						return true;
					}
				}
			}
		}
		return false;
	}

  private boolean isUnionTuple(StatementPattern stmt, List<TupleExpr> unionTupleExpr){
    for(TupleExpr tE : unionTupleExpr){
      FedXStatementPattern current = (FedXStatementPattern)tE;
      if(current.getSubjectVar().equals(stmt.getSubjectVar()) && current.getObjectVar().equals(stmt.getObjectVar()) &&  current.getPredicateVar().equals(stmt.getPredicateVar()) )
        return true;
    }
    return false;
  }

  public boolean checkRepeatedInitialBgps(ArrayList<ArrayList<Integer>> queryPlan){
    Set<Integer> initSet = new HashSet<>();
    for(ArrayList<Integer> qPlan : queryPlan){
      if(initSet.contains(qPlan.get(0)))
        return true;
      else{
        initSet.add(qPlan.get(0));
      }
    }
    return false;
  }

}
