package fr.inria.dyliss.grafeque.algebra;

import java.util.List;

import org.openrdf.query.algebra.QueryModelVisitor;
import org.openrdf.query.algebra.TupleExpr;

import com.fluidops.fedx.structures.QueryInfo;
import com.fluidops.fedx.algebra.NTuple;


/**
 * A tuple expression that represents an nary-Union designed for Grafeque.
 *
 * @author Vijay Ingalalli
 *
 */
public class NUnionG extends NTuple implements TupleExpr {

	/**
	 * Construct an nary-tuple. Note that the parentNode of all arguments is
	 * set to this instance.
	 *
	 * @param args
	 */
	public NUnionG(List<TupleExpr> args, QueryInfo queryInfo) {
		super(args, queryInfo);
	}

	@Override
	public <X extends Exception> void visit(QueryModelVisitor<X> visitor)
			throws X {
		visitor.meetOther(this);
	}

	@Override
	public NUnionG clone() {
		return (NUnionG)super.clone();
	}
}
