/*
 * @author Vijay Ingalalli
 */


package fr.inria.dyliss.grafeque.summary;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.TreeSet;

import java.io.FileOutputStream;
import java.io.OutputStream;

import fr.inria.dyliss.grafeque.util.GraphProperty;
import fr.inria.dyliss.grafeque.util.Pair;
import fr.inria.dyliss.grafeque.util.MultiEdge;
import fr.inria.dyliss.grafeque.summary.SummaryBuilder;



public class SummaryManager {


	protected void queryPredicateGraphSummary(){

	}


	public boolean queryNeighSummary(int K, GraphProperty bgpGraph, BitSet[][][] kNeighbors){ // public, since its accessed from other package
		SummaryBuilder queryBgp = new SummaryBuilder();
		queryBgp.buildNeighSummary(K, bgpGraph); // create matrix summary for queryBGP

/*
		System.out.println("Summary size: " + queryBgp.matrixSummary.size());
		for(Map.Entry<Integer, ArrayList<Map<Integer, Pair<BitSet, MutableInt>>>> summary : queryBgp.matrixSummary.entrySet()) {
			System.out.print(summary.getKey() + ": ");
			for(int k = 0; k < K; ++k) {
				for(Map.Entry<Integer, Pair<BitSet, MutableInt>> sA : summary.getValue().get(k).entrySet()) {
          // if(sA.getValue().first.isEmpty())
					System.out.print(sA.getKey() + ":" + sA.getValue().first + "," + sA.getValue().second.get() + "// ");
				}
				System.out.print("\t");
			}
			System.out.println();
		}
*/

		// Check if the queryBGP predicate is present in the corresponding ep summary.
    for(Map.Entry<Integer, ArrayList<Map<Integer, Pair<BitSet, MutableInt>>>> summary : queryBgp.matrixSummary.entrySet()) {
      int pred1 = summary.getKey();
      for(int k = 0; k < K; ++k) {
        for(Map.Entry<Integer, Pair<BitSet, MutableInt>> sA : summary.getValue().get(k).entrySet()) {
					int pred2 = sA.getKey();
					BitSet kNeigh = sA.getValue().first;
// System.out.println("Query kNeigh: " + kNeigh);
          if(!kNeigh.isEmpty()){ // there exists a 1-neighboorhood between 2 predicates
// System.out.println("Summary kNeigh: " + kNeighbors[pred1][pred2][0]);
						if(kNeigh.intersects(kNeighbors[pred1][pred2][0])){
							BitSet andSet = kNeighbors[pred1][pred2][0]; //.clone();
							andSet.and(kNeigh); // perform and operation bwtqeen query and summary bitet
// System.out.println("AND operation: " + andSet);
// System.out.println("Preds: " + pred1 + " , " + pred2);
							if(kNeigh.cardinality() != andSet.cardinality()){ // query bitset is not a subset of summary bitset
System.out.println("A NBGP FOUND!!!!!!!!!!!!!!!!!!");
								return false;
							}
						}
					}
        }
      }
    }
		return true;
	}
}
