/*
 * @author Vijay Ingalalli
 */


package fr.inria.dyliss.grafeque.summary;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.Queue;

import fr.inria.dyliss.grafeque.util.Pair;
import fr.inria.dyliss.grafeque.util.RdfSummary;
import fr.inria.dyliss.grafeque.util.MultiEdge;
import fr.inria.dyliss.grafeque.util.HashPair;

public class SummaryReader{

	public static	Map<String, RdfSummary> summaries = new HashMap<>();
	public static	Map<String, ArrayList<Integer>> predicateGraphSummary = new HashMap<>();

	public void readPredicateGraphSummary(String filePath) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		try{
			String line = br.readLine();
			while(line != null){
				String[] lineArray = line.split("\t");
				String edge = lineArray[0] + " " + lineArray[1];
				String trimmedEL = lineArray[2].substring(1, lineArray[2].length()-1);
				String[] eLabels = trimmedEL.split(", ");
				ArrayList<Integer> edgeLabels = new ArrayList<>();

				for(int i = 0; i < eLabels.length; ++i)
					edgeLabels.add(Integer.parseInt(eLabels[i]));

				if(predicateGraphSummary.containsKey(edge))
					predicateGraphSummary.get(edge).addAll(edgeLabels);
				else
					predicateGraphSummary.put(edge, edgeLabels);

				line = br.readLine();
			}
		}
		finally{
			br.close();
		}
	}

	public void readEpFile(String epPath, ArrayList<Pair<String, String>> endpoints)  throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(epPath));
		try{
			String line = br.readLine();
			while(line != null){
				if(line.trim().equals("")) // an empty line with/without spaces
					line = br.readLine();
				else {
					if(line.charAt(0) != '#'){ // not a comment
						String[] splitLine = line.split(" ");
						Pair<String, String> ep = new Pair(splitLine[0], splitLine[1]);
						endpoints.add(ep);
					}
					line = br.readLine();
				}
			}
		}
		finally {
			br.close();
		}
	}

	public void readSummaries(String pathName, ArrayList<String> endpoints){
		// 1. Read Neighbourhood summaries
		for(String ep : endpoints){
			RdfSummary rdfSummary = new RdfSummary();
			String folderName = pathName + ep + "/";
			try{
				readPredicateMapping(folderName + "edge_map.txt", rdfSummary);
			}
			catch(IOException e){
				e.printStackTrace();
			}
			readMatrixSummary(folderName, rdfSummary);
			summaries.put(ep, rdfSummary);
		}
		// 2. Read predicateGraph summary
		try{
			readPredicateGraphSummary(pathName+"predicateGraph.txt");
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	private void readPredicateMapping(String fileName, RdfSummary rdfSummary) throws IOException{
	  BufferedReader br = new BufferedReader(new FileReader(fileName));
	  try{
	    String line = br.readLine();
	    while(line != null){
	      String[] lineArray = line.split(" ");
	      rdfSummary.predicateMap.put(lineArray[0], Integer.parseInt(lineArray[1]));
	      line = br.readLine();
	    }
	  }
	  finally {
	    br.close();
	  }
	}

	private void readMatrixSummary(String summaryPath, RdfSummary rdfSummary){
	  try {
	    FileInputStream fileIn = new FileInputStream(summaryPath + "kNeighbors.ser");
	    ObjectInputStream in = new ObjectInputStream(fileIn);
	    rdfSummary.kNeighbours =  (BitSet[][][]) in.readObject();
	    in.close();
	    fileIn.close();
	    // System.out.println("Serialized data is read");
	  }
	  catch (IOException | ClassNotFoundException i) {
	    i.printStackTrace();
	    return;
	  }
	  try {
	    FileInputStream fileIn = new FileInputStream(summaryPath + "frequency.ser");
	    ObjectInputStream in = new ObjectInputStream(fileIn);
	    rdfSummary.kFrequencies =  (Integer[][][]) in.readObject();
	    in.close();
	    fileIn.close();
	    // System.out.println("Serialized data is read");
	  }
	  catch (IOException | ClassNotFoundException i) {
	    i.printStackTrace();
	    return;
	  }
	}
}
