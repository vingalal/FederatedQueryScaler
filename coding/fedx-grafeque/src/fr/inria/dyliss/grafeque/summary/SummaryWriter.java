package fr.inria.dyliss.grafeque.summary;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.Queue;

public class SummaryWriter{

  public static void writePredicateMappings(String summaryOutPath, List<String> predicates){
    try{
      PrintWriter writer = new PrintWriter(summaryOutPath, "UTF-8");
      int m = 0;
      for(String predicate : predicates){
      	if (!predicate.contains("openlinksw.com")){
      		writer.println("<" + predicate + ">" + " " + m);
          ++m;
        }
      }
      writer.close();
    }
    catch (IOException i) {
      i.printStackTrace();
    }
  }
}
