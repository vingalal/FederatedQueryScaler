/*
 * @author Vijay Ingalalli
 */


package fr.inria.dyliss.grafeque.summary;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
// import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.TreeSet;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;

import fr.inria.dyliss.grafeque.util.NodePair;
import fr.inria.dyliss.grafeque.util.GraphProperty;
import fr.inria.dyliss.grafeque.util.Pair;
import fr.inria.dyliss.grafeque.util.MultiEdge;

import fr.inria.dyliss.grafeque.summary.SummaryWriter;

class TimeEvaluation{
	static double time1 = 0;
	static double time2 = 0;
	static double time3 = 0;
	static double time4 = 0;
}


class GraphOperations{

	public static ArrayList<Set<Integer>> makeUnion(ArrayList<Set<Integer>> kNeighboursOld, ArrayList<Set<Integer>> kNeighboursNew){
		int minSize = Math.min(kNeighboursOld.size(), kNeighboursNew.size());
		ArrayList<Set<Integer>> result = new ArrayList<Set<Integer>>();
		if(kNeighboursOld.size() >= kNeighboursNew.size())
			result.addAll(kNeighboursOld);
		else
			result.addAll(kNeighboursNew);
		for(int k = 0; k < minSize; ++k){ // Make union for all k-neighbours
			if(kNeighboursOld.size() >= kNeighboursNew.size()){
				result.get(k).addAll(kNeighboursNew.get(k));
			}
			else{
				result.get(k).addAll(kNeighboursOld.get(k));
			}
		}
		return result;
	}

	public static ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> mergeMaps(ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> kAdjSummary, Map<Integer, Pair<BitSet, MutableInt>> adjSummary, int k){
		ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> kAdjSummaryNew = new ArrayList<>();
		int adjSize = kAdjSummary.size();
		// collect the unchangeable summary for values < k.
		for(int i = 0; i < k; ++i)
			kAdjSummaryNew.add(kAdjSummary.get(i));

		// Merge the values for 'k'
		Map<Integer, Pair<BitSet, MutableInt>> adjSummaryNew = kAdjSummary.get(k);
		for(Map.Entry<Integer, Pair<BitSet, MutableInt>> entry : adjSummary.entrySet()) {
			int pred = entry.getKey();
			if(adjSummaryNew.containsKey(entry.getKey())) {
				for(int b = 0; b < 4; ++b)
					if(entry.getValue().first.get(b))
						adjSummaryNew.get(pred).first.set(1);
				adjSummaryNew.get(pred).second.increment();
			}
			else {
				adjSummaryNew.put(pred, entry.getValue());
			}
		}
		kAdjSummaryNew.add(adjSummaryNew); // kth modified/merged element

		// collect the unchangeable summary for values >k.
		for(int i = k+1; i < adjSize; ++i)
			kAdjSummaryNew.add(kAdjSummary.get(i));

		return kAdjSummary;
	}

	public static void getAdjList(GraphProperty graphData, int nNodes){
		graphData.adjList.ensureCapacity(nNodes);
    Set<Integer> adjNull = new HashSet<Integer>();
    for(int i  = 0; i < nNodes; ++i)
    	graphData.adjList.add(adjNull);

    for(Map.Entry<NodePair, ArrayList<Integer>> entry : graphData.edges.entrySet()){
  	// A node with incoming edge is positive, and a node with outgoing edge is negative.
  	int nodeS = entry.getKey().getFirst();
  	int nodeO = entry.getKey().getSecond();
  	if(graphData.adjList.get(nodeS).isEmpty()){ // adding a fresh adjacent node
    	Set<Integer> adj = new HashSet<Integer>();
    	adj.add(nodeO);
    	graphData.adjList.set(nodeS, adj);
  	}
  	else{ // at least one adjacent node already exists
  		Set<Integer> adj = graphData.adjList.get(nodeS);
  		adj.add(nodeO);
  		graphData.adjList.set(nodeS, adj);
  	}
  	if(graphData.adjList.get(nodeO).isEmpty()){ // adding a fresh adjacent node
    	Set<Integer> adj = new HashSet<Integer>();
    	adj.add(nodeS + nNodes);
    	graphData.adjList.set(nodeO, adj);
  	}
  	else{ // at least one adjacent node already exists
  		Set<Integer> adj = graphData.adjList.get(nodeO);
  		adj.add(nodeS + nNodes);
  		graphData.adjList.set(nodeO, adj);
  	}
  }

  /// Output adjacent list size for few vertices
	Set<Integer> adjListSize = new TreeSet<Integer>();
	int avgDegree = 0;
	for(int i = 0; i < nNodes; ++ i){
		adjListSize.add(graphData.adjList.get(i).size());
		avgDegree+=graphData.adjList.get(i).size();
	}
	LinkedList<Integer> list = new LinkedList<>(adjListSize);
	Iterator<Integer> itr = list.descendingIterator();
	int count = 0;
	while(itr.hasNext()) {
		System.out.print(itr.next() + ", ");
		if (count > 10)
		break;
		++count;
	}
	// System.out.println();
	// System.out.println("Average node degree: " + (double)avgDegree/nNodes);
	}
}


class MyStack2D<T> {
//	private int maxRow;
	private ArrayList<ArrayList<T>> stackArray;
//	private int row;
	private int[] pointer;

	public MyStack2D(int stackSize) {
//		ArrayList<ArrayList<T>> stackArray = new ArrayList<ArrayList<T>>(maxRow);
		stackArray = new ArrayList<ArrayList<T>>(stackSize);
		pointer = new int[stackSize];
		ArrayList<T> emptyElement = new ArrayList<T>();
//		row = -1;
		for(int i = 0 ; i < stackSize; ++i){
			stackArray.add(emptyElement); // initialize with empty ArrayList<T>
			pointer[i] = -1;
		}
	}
	public void push(ArrayList<T> stackElement, int r) { // An array is pushed onto stack
		stackArray.set(r, stackElement);
		pointer[r] = stackElement.size()-1;
	}
	public T pop(int r) { // An element from 'r'th row from stackArray is popped
//		System.out.println("pop-depth: " + r + " : " + stackArray.get(r).size() + "::" + column[r]);
	  return stackArray.get(r).get(pointer[r]--);
	}
	public T peek(int r) {
	  return stackArray.get(r).get(pointer[r]);
	}
	public boolean isEmpty(int r) {
		return (pointer[r] == -1);
	}
	// Functions for debugging
	public int getSize(int r) {
	  return stackArray.get(r).size();
	}
	public T getEdges(int r, int i) {
	  return stackArray.get(r).get(i);
	}
}

class MutableInt {
  int value = 1; // initialize count =1
  public void increment() { ++value; }
  public int get() { return value; }
}

public class SummaryBuilder {

	protected Map<Integer, ArrayList<Map<Integer, Pair<BitSet, MutableInt>>>> matrixSummary = new HashMap<>();

	private void updateMatrixSummary(int k, ArrayList<Integer> inPredicates, Map<Integer, Pair<BitSet, MutableInt>> adjSummary) {

		for(int p = 0; p < inPredicates.size(); ++p){
			int pred = inPredicates.get(p);
			if(!matrixSummary.containsKey(pred)) {
				ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> adjPredicateSummary = new ArrayList<>();
				adjPredicateSummary.add(adjSummary);
				matrixSummary.put(pred, adjPredicateSummary);

			}
			else { // predicate already visited
				if(matrixSummary.get(pred).size() <= k) { // adj predicates for k=0 exist for sure; however k>=1 needs to be checked
					matrixSummary.get(pred).add(adjSummary);
				}
				else { // adj predicate already exist for any value of k. Thus, Union operation is to be performed.
					matrixSummary.replace(pred, GraphOperations.mergeMaps(matrixSummary.get(pred), adjSummary, k));
				}
			}
		}

	}

	private static void updateAdjSummary(Map<Integer, Pair<BitSet, MutableInt>> adjSummary, int adjPredicate, int join) {

		if(adjSummary.containsKey(adjPredicate)) {
			adjSummary.get(adjPredicate).first.set(join);
			adjSummary.get(adjPredicate).second.increment();
		}
		else {
			BitSet joins = new BitSet(4);
			joins.set(join);
			Pair<BitSet, MutableInt> newAdj = new Pair<BitSet, MutableInt>(joins, new MutableInt());
			adjSummary.put(adjPredicate, newAdj);
		}
	}

	public static void getAdjPredicates(Set<Integer> adjList, int node, GraphProperty rdfGraph, Map<Integer, Pair<BitSet, MutableInt>> adjSummary){
		int nNodes = rdfGraph.adjList.size();
		/*
		 * Convention on the join operations that happens among a pair of predicates (input predicate, corresponding adjacent predicate)
		 * input-P	adjacent-P
		 * object		subject	=	0
		 * subject	subject	=	1
		 * object		object	= 2
		 * subject	object	=	3
		 * */
		for(Integer adjNode : adjList){
			if(adjNode < nNodes){ // 'adjNode' is Object and 'node' may be Subject/Object. => you are loosing a predicate, and hence '-predicate'
				try{
//System.out.println(node + "," + adjNode);
//System.out.println("1. Predicate Exists!");
					if(node < nNodes){ // 'node' is an object
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(node, adjNode);
						NodePair nP = new NodePair(node, adjNode);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 0); // when Source and Object are considered : second
//							updateAdjSummary(adjSummary, predicates.get(p) + rdfGraph.uniqueEdges.size(), 0); // when Source and Object are considered : second
						}
					}
					else{ // 'node' is a subject
//System.out.println(node + "," + adjNode);
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(node-nNodes, adjNode);
						NodePair nP = new NodePair(node-nNodes, adjNode);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 1); // when Source and Object are considered : second
//							updateAdjSummary(adjSummary, predicates.get(p) + rdfGraph.uniqueEdges.size(), 1); // when Source and Object are considered : second
						}
					}
				}
				catch(NullPointerException e) { // This exception should never be thrown
//System.out.println(node + "," + adjNode);
					System.out.println("1. Predicate does not exist in RDF");
				}
			}
			else{ // 'adjNode' is Subject and 'node' may be Subject/Object => you are gaining a predicate, and hence '+predicate'
				try{
//System.out.println(adjNode + "," + node);
//System.out.println("2. Predicate Exists!");
					if(node < nNodes){ // 'node' is an object
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(adjNode-nNodes, node);
						NodePair nP = new NodePair(adjNode-nNodes, node);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 2); // when Source and Object are considered : second
						}
					}
					else{ // 'node' is a subject
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(adjNode-nNodes, node-nNodes);
						NodePair nP = new NodePair(adjNode-nNodes, node-nNodes);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 3);
						}
					}
				}
				catch(NullPointerException e) { // This exception should never be thrown
//System.out.println(adjNode-nNodes + "," + node);
					System.out.println("2. Predicate does not exist in RDF");
				}
			}
		}
	}

	public void findKthNeighbours(Set<Integer> inputNodes, Set<Integer> alreadyVisitedNodes, Set<Integer> allAdjNodes, ArrayList<Integer> inPredicates, int k, GraphProperty rdfGraph){

		Map<Integer, Pair<BitSet, MutableInt>> adjSummary = new HashMap<Integer, Pair<BitSet, MutableInt>>(); // key = adj predicate; value = <join order, frequency>

		int nNodes = rdfGraph.adjList.size();
		for(Integer node : inputNodes){
			Set<Integer> adjNodes = new HashSet<Integer>();
			double startTime = System.currentTimeMillis();

			if(node < nNodes)
				adjNodes.addAll(rdfGraph.adjList.get(node));
			else
				adjNodes.addAll(rdfGraph.adjList.get(node-nNodes));

			Set<Integer> alreadyVisitedNodesDir = new HashSet<Integer>();
			for(Integer i : alreadyVisitedNodes){
				if(i < nNodes)
					alreadyVisitedNodesDir.add(i+nNodes);
				else
					alreadyVisitedNodesDir.add(i-nNodes);
			}
			adjNodes.removeAll(alreadyVisitedNodesDir);
			adjNodes.remove(node); // self loops are excluded
			adjNodes.removeAll(alreadyVisitedNodes);
  		TimeEvaluation.time4+= System.currentTimeMillis()-startTime;

			startTime = System.currentTimeMillis();
			getAdjPredicates(adjNodes, node, rdfGraph, adjSummary);
  		TimeEvaluation.time3+= System.currentTimeMillis()-startTime;
			allAdjNodes.addAll(adjNodes);
		}
		updateMatrixSummary(k, inPredicates, adjSummary);

//System.out.println(allAdjNodes.size());
		/*
		 * When the graph has triangles, the adjacent vertices of 'inputNodes' may consist of 'inputNodesDir'.
		 * These opposite directed nodes have to be removed for the updated 'inputNodes' that are used for next iteration.
		 * Note that 'inputNodesDir' have to be removed only after running 'getAdjPredicates'; otherwise we loose a predicate in the triangle.
		 * Any other possible repeated nodes are automatically removed by maintaining 'alreadyVisitedNodes'.
		 * */
		if(allAdjNodes.isEmpty())
			return;

		Set<Integer> inputNodesDir = new HashSet<Integer>();
		for(Integer node : inputNodes){
			if(node < nNodes)
				inputNodesDir.add(nNodes + node);
			else
				inputNodesDir.add(node - nNodes);
		}
		allAdjNodes.removeAll(inputNodesDir);
	}

	public void buildNeighSummary(int K, GraphProperty rdfGraph){

		int count = 0;
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : rdfGraph.edges.entrySet()){
    	int nodeS = entry.getKey().getFirst();
    	int nodeO = entry.getKey().getSecond();

    	Set<Integer> inputNodes = new HashSet<Integer>(Arrays.asList(nodeS+rdfGraph.adjList.size(), nodeO));
    	Set<Integer> alreadyVisitedNodes = new HashSet<Integer>(Arrays.asList(nodeS, nodeO, nodeS+rdfGraph.adjList.size())); // This is true only in the beginning.


  		int k = 0;
    	while(k < K && !inputNodes.isEmpty()){
				Set<Integer> allAdjNodes = new HashSet<Integer>();
				double startTime = System.currentTimeMillis();
    		findKthNeighbours(inputNodes, alreadyVisitedNodes, allAdjNodes, entry.getValue(), k, rdfGraph);
    		TimeEvaluation.time1+= System.currentTimeMillis()-startTime;
    		alreadyVisitedNodes.addAll(inputNodes);
    		inputNodes.clear();
    		inputNodes.addAll(allAdjNodes);
    		++k;
    	}

			++count;
			if((count % 100000) == 0){
				System.out.println(count);
			//    	break;
			}
		}
	}

//##############################################################################################################################################################################
//##############################################################################################################################################################################

	// Build Predicate Graph Summary

	// TODO For 'uriRepository' maintain a tree data structure OR multilvel/nested hashmap to avoid repreated parts of an uri
	private static final HashMap<String, Integer> uriRepository = new HashMap<>();
	private Integer uriCount = 0;
	private static final ArrayList<ArrayList<Integer>> pgEdgeLabels = new ArrayList<>();
	private HashMap<String, HashSet<Integer>> predicateGraph = new HashMap<>();

	public SummaryBuilder(){
		/**
		 Convention for labelling the predicateGraph edges
		 * subject-subject : 1
		 * subject-object : 2
		 * object-subject : 3
		 * object-object : 4
		 */
		ArrayList<Integer> tmp = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(1,3));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(2,4));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(1,2));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(1));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(2));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(3,4));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(3));
		this.pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(4));
		this.pgEdgeLabels.add(tmp);
	}


	/** @TODO Other possibile implementation of createPredGraphSummary().
	 * (I) Using trie structures
	 *	1.	For the initial endpoint, and for each predicate, create a subject-resourice-tree (srt) and an object-resource-tree (ort).
	 *	2.	For each subsequent endpoints:
	 *		- For each predicate, check if subject or object matches with the previously built srt/ort of each predicate and endpoint
	 *		-	This is how the predicate graph is built, and it grows as we cover all the endpoints.
	 *		-	Build srt and ort for the current endpoints, and store for matching to be done by successive endpoinits.
	 *	3. Predicate graph is built as a k-bipartite graph, where 'k' indicates the number of endpoint pairs <e_i, e_j>, that have at least one edge between a pair of predicates.
	 *
	 * (II) Using Map and partial URIs
	 *	1. Maintain a map of <uniquePredicates, Integer>, where uniquePredicates=<predicate:endpoint>, across all the endpoints. Read from SummaryReader.summaries.
	 *	2. Maintain lexicographic ordering of the keys in the map.
	 *	3. Fetch all the edges that are connected between a pair of endpoints.
	 *		- Exploit authorities for the ep pairs for faster checking. Then increase the levels in case of any existing matches.
	 *		- In case of matchings, fetch the corresponding predicates from each endpoints.
	 */

	/** Current implementation of createPredGraphSummary().
	 *	1. Maintain a main index 'uriRepository' of all URIs across all endpoints: Map<String URI, mapped int_id>.
	 *		- @TODO Maintain a suffix tree structure for much more compressed representation.
	 *	2. For any endpoint, any new URI (subject/object) is added to 'uriRepository'. At the same time, 'endpointResources' is maintained for each endpoint.
	 *		- For each endpoint, all the predicate information is stored in 'resources': Map<String predicate, Map<id, type>>.
	 *
	 *
	 */

	// collect resources from each endpoint
	public void createPredGraphSummary(ArrayList<Pair<String, String>> endpoints, String summaryOutPath){

		// ArrayList<MultiEdge> predicateGraph = new ArrayList<>();

		ArrayList<HashMap<String, ArrayList<HashSet<Integer>>>> endpointResources = new ArrayList<>();
		for(Pair<String, String> endpoint : endpoints){
			System.out.println("Processing Endpoint: " + endpoint.second);
			HashMap<String, ArrayList<HashSet<Integer>>> resources = new HashMap<>();
			try{
				getEndpointResources(endpoint, resources, summaryOutPath);
			}
			catch(Exception RepositoryException){
				System.out.println("Error");
			}
			endpointResources.add(resources);
			System.out.println("# uriRepository: " + uriRepository.size());
		}

		// Build predicate graph
	 for(int i = 0; i < endpointResources.size()-1; ++i){
		 for(int j = i + 1; j < endpointResources.size(); ++j){
			 for(Map.Entry<String, ArrayList<HashSet<Integer>>> pred_1 : endpointResources.get(i).entrySet()){
				 for(Map.Entry<String, ArrayList<HashSet<Integer>>> pred_2 : endpointResources.get(j).entrySet()){
					 for(int k = 0; k < pred_1.getValue().size(); ++k){
						 if(!Collections.disjoint(pred_1.getValue().get(k), pred_2.getValue().get(0))){
							 addEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+0));
							 // MultiEdge<String, String, ArrayList<Integer>> multiEdge = new MultiEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+0));
							 // predicateGraph.add(multiEdge);
							 if(k == 0)
							 	break; // all possible lables have been considered, since k =0 corresponds to labels [1234]
							 else
							 	continue; // all possible labels for the current 'k' have been considered.
						 }
						 else if(!Collections.disjoint(pred_1.getValue().get(k), pred_2.getValue().get(1))){
							 addEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+1));
							 // MultiEdge<String, String, ArrayList<Integer>> multiEdge = new MultiEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+1));
							 // predicateGraph.add(multiEdge);
						 }
						 else if(!Collections.disjoint(pred_1.getValue().get(k), pred_2.getValue().get(2))){
							 addEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+2));
							 // MultiEdge<String, String, ArrayList<Integer>> multiEdge = new MultiEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+2));
							 // predicateGraph.add(multiEdge);
						 }
					 }
				 }
			 }
			 System.out.println("PREDICATE GRAPH SIZE: " + predicateGraph.size());
	 		}
		}

		try{
			PrintWriter writer = new PrintWriter(summaryOutPath + "predicateGraph.txt", "UTF-8");
			for(Map.Entry<String, HashSet<Integer>> entry : predicateGraph.entrySet())
			// for(int i = 0; i < predicateGraph.size(); ++i)
				writer.println(entry.getKey() + "\t" + entry.getValue());
			writer.close();
		}
		catch (IOException i) {
			i.printStackTrace();
		}

		/*
		try{
			PrintWriter writer = new PrintWriter(summaryOutPath + "predicateGraph.txt", "UTF-8");
			for(int i = 0; i < predicateGraph.size(); ++i)
				writer.println(predicateGraph.get(i).first + "\t" + predicateGraph.get(i).second + "\t" + predicateGraph.get(i).mlbl);
			writer.close();
		}
		catch (IOException i) {
			i.printStackTrace();
		}
		*/
	}

	private void addEdge(String nodeS, String nodeO, ArrayList<Integer> labels){
		String edge = nodeS + "\t" + nodeO;
		HashSet<Integer> labelSet = new HashSet<>(labels);
		if(predicateGraph.containsKey(edge))
			predicateGraph.get(edge).addAll(labelSet);
		else
			predicateGraph.put(edge, labelSet);
	}

	private List<String> retrievePredicates(RepositoryConnection con) throws RepositoryException, MalformedQueryException, QueryEvaluationException {
		String rQ = "SELECT (COUNT(?s) AS ?triples) WHERE { ?s ?p ?o }";
		TupleQuery qry = con.prepareTupleQuery(QueryLanguage.SPARQL, rQ);
		TupleQueryResult res = qry.evaluate();
		Long cnt = Long.parseLong(res.next().getValue("triples").stringValue());
		System.out.println("#Triples: " + cnt);

		String rawQuery = "SELECT distinct ?p WHERE { ?s ?p ?o }";
		TupleQuery query = con.prepareTupleQuery(QueryLanguage.SPARQL, rawQuery);
		TupleQueryResult result = query.evaluate();
		List<String> predicateSet = new ArrayList<String>();
		while (result.hasNext()) {
			BindingSet binds = result.next();
			Value predicate = binds.getValue("p");
			predicateSet.add(predicate.toString());
		}
		return predicateSet;
	}

	private void getEndpointResources(Pair<String, String> endpoint, HashMap<String, ArrayList<HashSet<Integer>>> resources, String summaryOutPath) throws RepositoryException, MalformedQueryException, QueryEvaluationException{

		// 0.0	Get all the predicates of an endpoint
		SPARQLRepository repo = new SPARQLRepository(endpoint.first);
		repo.initialize();
		RepositoryConnection conn = repo.getConnection();

		List<String> predicates = retrievePredicates(conn);
		System.out.println("# distinct predicates: " + predicates.size());
	  SummaryWriter.writePredicateMappings(summaryOutPath + endpoint.second +  ".txt", predicates);


		//	1.1	Building predicate graph using HashMap implementation
		int usedPreds = 0;
		for(String predicate : predicates){
			if (predicate.contains("openlinksw.com"))
				continue;

			// Get unique subject resources
			HashSet<Integer> subjects = new HashSet<>(); // Using HashSet and ArrayList has same time performance.
			// String subjQuery = "SELECT DISTINCT ?s WHERE {?s <"+predicate+"> ?o. FILTER isURI(?s)}";
			String subjQuery = "SELECT DISTINCT ?s WHERE {?s <"+predicate+"> ?o.}"; // DISTINCT drastically reduces time.

			TupleQueryResult subRes = conn.prepareTupleQuery(QueryLanguage.SPARQL, subjQuery).evaluate();
			while(subRes.hasNext()) {
				BindingSet binds = subRes.next();
				String subject = binds.getValue("s").stringValue();
				if(subject.startsWith("http") && subject.contains("<www.w3.org/2001/XMLSchema#string>") &&  subject.contains("localhost:8890/"))
					continue; // these resources need not be processed.
				if(!uriRepository.containsKey(subject)){ // uri doesn't exist yet in the repository.
					uriRepository.put(subject, uriCount);
					++uriCount;
					subjects.add(uriCount); // a new subject uri
				}
				else
					subjects.add(uriRepository.get(subject)); // the current subject uri was already visited in some endpoint/predicate before.
			}

			// Get unique object resources
			HashSet<Integer> objects = new HashSet<>(); // Using HashSet and ArrayList has same time performance.
			HashSet<Integer> subjectsAndObjects = new HashSet<>();
			// String objQuery = "SELECT DISTINCT ?o WHERE {?s <"+predicate+"> ?o. FILTER isURI(?o)}";
			String objQuery = "SELECT DISTINCT ?o WHERE {?s <"+predicate+"> ?o.}"; // DISTINCT drastically reduces time.

			TupleQueryResult objRes = conn.prepareTupleQuery(QueryLanguage.SPARQL, objQuery).evaluate();
			while(objRes.hasNext()) {
				BindingSet binds = objRes.next();
				String object = binds.getValue("o").stringValue();
				if(object.startsWith("http") && object.contains("<www.w3.org/2001/XMLSchema#string>") &&  object.contains("localhost:8890/"))
					continue; // these resources need not be processed.
				if(!uriRepository.containsKey(object)){ // uri doesn't exist yet in the repository.
					uriRepository.put(object, uriCount);
					++uriCount;
					objects.add(uriCount); // a new object uri
				}
				else{
					int uriId = uriRepository.get(object);
					if(subjects.contains(uriId))
						subjectsAndObjects.add(uriId); // the current object uri is also a subject uri for the chosen predicate p_i and endpoint e_n
						else
						objects.add(uriId); // the current object uri is purely an object uri, which was already visited in some endpoint/predicate before.
				}
			}

			subjects.removeAll(subjectsAndObjects); // 'subjects' now contains only those uris that are uniquely subject
			if(!subjects.isEmpty() || !objects.isEmpty() || !subjectsAndObjects.isEmpty()){
				ArrayList<HashSet<Integer>> subjObjResources = new ArrayList<>();
				subjObjResources.add(subjectsAndObjects);
				subjObjResources.add(subjects);
				subjObjResources.add(objects);
				resources.put(predicate + ":" + endpoint.second, subjObjResources);
				usedPreds++;
			}
			System.out.print(usedPreds + ", ");
		}

		System.out.println();
		System.out.println("# Predicates considered for PG: " + resources.size());

		conn.close();
		repo.shutDown();
	}
}
