package fr.inria.dyliss.grafeque.planoptimizer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import org.apache.log4j.Logger;
import org.openrdf.query.algebra.QueryModelNode;
import org.openrdf.query.algebra.Service;
import org.openrdf.query.algebra.StatementPattern;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.helpers.QueryModelVisitorBase;

import com.fluidops.fedx.algebra.EmptyNJoin;
import com.fluidops.fedx.algebra.EmptyResult;
import com.fluidops.fedx.algebra.ExclusiveGroup;
import com.fluidops.fedx.algebra.ExclusiveStatement;
import com.fluidops.fedx.algebra.StatementSourcePattern;
import com.fluidops.fedx.algebra.NJoin;
import com.fluidops.fedx.algebra.NUnion;
import com.fluidops.fedx.algebra.TrueStatementPattern;
import com.fluidops.fedx.exception.OptimizationException;
import com.fluidops.fedx.structures.QueryInfo;
import com.fluidops.fedx.util.QueryStringUtil;

import com.fluidops.fedx.optimizer.FedXOptimizer;
import com.fluidops.fedx.optimizer.JoinOrderOptimizer;

import fr.inria.dyliss.grafeque.util.Pair;
import fr.inria.dyliss.grafeque.graph.BgpQueryPlanner;
import fr.inria.dyliss.grafeque.algebra.NUnionG;

/**
 * Optimizer with the following tasks:
 *
 * 1. Group {@link ExclusiveStatement} into {@link ExclusiveGroup}
 * 2. Adjust the join order using {@link JoinOrderOptimizer}
 *
 *
 * @author as
 */
public class BgpOptimizer extends QueryModelVisitorBase<OptimizationException> implements FedXOptimizer {

	public static Logger log = Logger.getLogger(BgpOptimizer.class);

	protected final QueryInfo queryInfo;

	public BgpOptimizer(QueryInfo queryInfo) {
		super();
		this.queryInfo = queryInfo;
	}



	@Override
	public void optimize(TupleExpr tupleExpr) {
		tupleExpr.visit(this);
	}

	@Override
	public void meet(Service tupleExpr) {
		// stop traversal
	}


	@Override
	public void meetOther(QueryModelNode node) {
		if (node instanceof NJoin) {
			super.meetOther(node);		// depth first
			// meetNJoin((NJoin) node);
      meetNExpr((NJoin) node);
		} else {
			super.meetOther(node);
		}
	}

/*
 	protected void groupPermutations(){
		// Collect unique BGPs and theis corresponding endpoints
		HashMap<ArrayList<Integer>, HashSet<String>> uniquePrunedPermutations = new HashMap<>();
		for(int i = 0; i < BgpQueryPlanner.prunedBgpPermutations.size(); ++i){
			for(Pair<String, ArrayList<Integer>> element : BgpQueryPlanner.prunedBgpPermutations.get(i)){
				if(uniquePrunedPermutations.containsKey(element.second))
					uniquePrunedPermutations.get(element.second).add(element.first.replace("sparql_",""));
				else{
					HashSet<String> tmp = new HashSet<>();
					tmp.add(element.first.replace("sparql_",""));
					uniquePrunedPermutations.put(element.second, tmp);
				}
			}
		}
		System.out.println("uniquePrunedPermutations: " + uniquePrunedPermutations.size());

		// Aggregate the BGPs into "ExclusiveGroup" and "StatementSourcePattern"
		ArrayList<ArrayList<Pair<ArrayList<Integer>, HashSet<String>>>> groupedBGP = new ArrayList<>(); // store map into an arrayList
		ArrayList<Pair<ArrayList<Integer>, HashSet<String>>> arrayMap = new ArrayList<>();
		for(Map.Entry<ArrayList<Integer>, HashSet<String>> entry: uniquePrunedPermutations.entrySet()){
			Pair<ArrayList<Integer>, HashSet<String>> bgpPair = new  Pair(entry.getKey(), entry.getValue());
			arrayMap.add(bgpPair);
		}
		for(int i = 0 ; i < arrayMap.size()-1; ++i){
			int s = 0;
			for(int j = i+1; j < arrayMap.size(); ++j){
				s+=arrayMap.get(i).getFirst.size() + arrayMap.get(j).getFirst.size();
				if(s == nTriples){ // all the triples have been considered
					// collect the combination
					break; // break the j loop
				}
				else if (s < nTriples)
					continue; // continue with more combination from 'j'
				else
					break; // doesnt exits a comnbonation
			}
		}
	}
*/

  protected void meetNExpr(NJoin node) {
    System.out.println("prunedBgpPermutations.size(): " + BgpQueryPlanner.prunedBgpPermutations.size());

		// groupPermutations();

		// Map<ArrayList<Integer>, HashSet<String>> invalidBgpPermutations = new HashMap<>();



		LinkedList<TupleExpr> newArgs = new LinkedList<TupleExpr>();
		LinkedList<TupleExpr> argsCopy = new LinkedList<TupleExpr>(node.getArgs());


// System.out.println("args copy: " + argsCopy);

// System.out.println("Permutation size: " + BgpQueryPlanner.prunedBgpPermutations.size());

    // ArrayList<ArrayList<Pair<String, ArrayList<Integer>>>> prunedBgpPermutations;
    for(int i = 0; i < BgpQueryPlanner.prunedBgpPermutations.size(); ++i){ // for each BGP permutation
      for(int j = 0; j < BgpQueryPlanner.prunedBgpPermutations.get(i).size(); ++j){ // for each BGP
        ArrayList<Integer> bgp = BgpQueryPlanner.prunedBgpPermutations.get(i).get(j).second; // BGP with triple ids
  // System.out.println("bgp: " + bgp);
        List<ExclusiveStatement> l = new ArrayList<>();


/*
				if (sources.size()>1) {
					StatementSourcePattern stmtNode = new StatementSourcePattern(stmt, queryInfo);
					for (StatementSource s : sources)
						stmtNode.addStatementSource(s);
					stmt.replaceWith(stmtNode);
				}

				else if (sources.size()==1) {
					stmt.replaceWith( new ExclusiveStatement(stmt, sources.get(0), queryInfo));
				}



*/


        ExclusiveStatement tInit = (ExclusiveStatement)argsCopy.get(bgp.get(0)); // =  new ExclusiveStatement();
        if(bgp.size() > 1){ // Form an exclusive group of any kind (ExclusiveStatement/StatementSourcePattern/etc.) of statement.
          for(Integer k : bgp){
            TupleExpr t = argsCopy.get(k); // k^th triple of the BGP
            ExclusiveStatement kT;
            kT = (ExclusiveStatement)t; // all triples are forced to be 'ExclusiveStatement' in 'SourceSelection.java'
            l.add(kT);
          }
          newArgs.add( new ExclusiveGroup(l, tInit.getOwner(), queryInfo)); // current.getOwner() is the source of the ExclusiveGroup
        }
        else
          newArgs.add( tInit );
      }
    }

    System.out.println("newArgs Grafeque: " + newArgs);

		// if the join args could be reduced to just one, e.g. OwnedGroup
		// we can safely replace the join node
		if (newArgs.size()==1) {
			log.debug("Join arguments could be reduced to a single argument, replacing join node.");
			node.replaceWith( newArgs.get(0) );
			return;
		}

		// in rare cases the join args can be reduced to 0, e.g. if all statements are
		// TrueStatementPatterns. We can safely replace the join node in such case
		if (newArgs.size()==0) {
			log.debug("Join could be pruned as all join statements evaluate to true, replacing join with true node.");
			node.replaceWith( new TrueStatementPattern( new StatementPattern()));
			return;
		}

		List<TupleExpr> optimized = newArgs;

		// optimize the join order
		optimized = JoinOrderOptimizer.optimizeJoinOrder(optimized);

// System.out.println("optimized: " + optimized);

		// exchange the node
    NJoin newNode = new NJoin(optimized, queryInfo);
		node.replaceWith(newNode);
    // NUnion newUNode = new NUnion(optimized, queryInfo);
		// node.replaceWith(newUNode);

		// Generate a query plan with Unions and Joins
		// collectUnionPerms(join, optimized);
		// return new NUnion(optimized, queryInfo);

  }

	protected void meetNJoin(NJoin node) {

    LinkedList<TupleExpr> newArgs = new LinkedList<TupleExpr>();
		LinkedList<TupleExpr> argsCopy = new LinkedList<TupleExpr>(node.getArgs());

		while (!argsCopy.isEmpty()) {

			TupleExpr t = argsCopy.removeFirst();

			/*
			 * If one of the join arguments cannot produce results,
			 * the whole join expression does not produce results.
			 * => replace with empty join and return
			 */
			if (t instanceof EmptyResult) {
				node.replaceWith( new EmptyNJoin(node, queryInfo));
				return;
			}

			/*
			 * for exclusive statements find those belonging to the
			 * same source (if any) and form exclusive group
			 */
			else if (t instanceof ExclusiveStatement) {
System.out.println("HAS: ExclusiveStatement" );

				ExclusiveStatement current = (ExclusiveStatement)t;

				List<ExclusiveStatement> l = null;
				for (TupleExpr te : argsCopy) {
					/* in the remaining join args find exclusive statements
					 * having the same source, and add to a list which is
					 * later used to form an exclusive group
					 */
					if (te instanceof ExclusiveStatement) {
						ExclusiveStatement check = (ExclusiveStatement)te;
						if (check.getOwner().equals(current.getOwner())) {
							if (l==null) {
								l = new ArrayList<ExclusiveStatement>();
								l.add(current);
							}
							l.add(check);
						}
					}
				}


				// check if we can construct a group, otherwise add directly
				if (l!=null) {
					argsCopy.removeAll(l);
System.out.println("current.getOwner(): " + current.getOwner());

					newArgs.add( new ExclusiveGroup(l, current.getOwner(), queryInfo ));
				} else {
					newArgs.add( current );
				}
			}

			/*
			 * statement yields true in any case, not needed for join
			 */
			else if (t instanceof TrueStatementPattern) {
System.out.println("HAS: TrueStatementPattern" );
				if (log.isDebugEnabled())
					log.debug("Statement " + QueryStringUtil.toString((StatementPattern)t) + " yields results for at least one provided source, prune it.");
			}

			else{ // has StatementSourcePattern
System.out.println("HAS: StatementSourcePattern" );
				newArgs.add(t);
      }
		}


// System.out.println("newArgs: " + newArgs);

		// if the join args could be reduced to just one, e.g. OwnedGroup
		// we can safely replace the join node
		if (newArgs.size()==1) {
			log.debug("Join arguments could be reduced to a single argument, replacing join node.");
			node.replaceWith( newArgs.get(0) );
			return;
		}

		// in rare cases the join args can be reduced to 0, e.g. if all statements are
		// TrueStatementPatterns. We can safely replace the join node in such case
		if (newArgs.size()==0) {
			log.debug("Join could be pruned as all join statements evaluate to true, replacing join with true node.");
			node.replaceWith( new TrueStatementPattern( new StatementPattern()));
			return;
		}

		List<TupleExpr> optimized = newArgs;

		// optimize the join order
		optimized = JoinOrderOptimizer.optimizeJoinOrder(optimized);

// System.out.println("optimized: " + optimized);

		// exchange the node
		NJoin newNode = new NJoin(optimized, queryInfo);
		node.replaceWith(newNode);
	}

}
