/*
 * Copyright (C) 2008-2013, fluid Operations AG
 *
 * FedX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.inria.dyliss.grafeque.planoptimizer;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;

import org.aksw.simba.start.QueryEvaluation;
import org.apache.log4j.Logger;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.algebra.QueryRoot;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.evaluation.impl.ConstantOptimizer;
import org.openrdf.query.algebra.evaluation.impl.DisjunctiveConstraintOptimizer;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStrategyImpl;
import org.openrdf.sail.SailException;
import org.openrdf.query.algebra.StatementPattern;

import com.fluidops.fedx.FedX;
import com.fluidops.fedx.FederationManager;
import com.fluidops.fedx.algebra.SingleSourceQuery;
import com.fluidops.fedx.cache.Cache;
import com.fluidops.fedx.structures.Endpoint;
import com.fluidops.fedx.structures.QueryInfo;

import com.fluidops.fedx.optimizer.*;

import fr.inria.dyliss.grafeque.graph.BgpQueryPlanner;
import fr.inria.dyliss.grafeque.planoptimizer.BgpOptimizer;

public class Optimizer extends com.fluidops.fedx.optimizer.Optimizer {

	public static Logger logger = Logger.getLogger(Optimizer.class);
	public static List<StatementPattern> statementPatterns = new ArrayList<>();

	// public static GenericInfoOptimizer info = new GenericInfoOptimizer();

	public static TupleExpr optimize(TupleExpr parsed, Dataset dataset, BindingSet bindings,
			EvaluationStrategyImpl strategy, QueryInfo queryInfo) throws SailException {


		FedX fed = FederationManager.getInstance().getFederation();
		List<Endpoint> members = fed.getMembers();

		// if the federation has a single member only, evaluate the entire query there
		if (members.size()==1 && queryInfo.getQuery()!=null)
			return new SingleSourceQuery(parsed, members.get(0), queryInfo);

		// Clone the tuple expression to allow for more aggressive optimizations
		TupleExpr query = new QueryRoot(parsed.clone());

// System.out.println("query at the beginning: " + query);

		Cache cache = FederationManager.getInstance().getCache();

		//if (logger.isTraceEnabled())
		//	logger.trace("Query before Optimization: " + query);


		/* original sesame optimizers */
		new ConstantOptimizer(strategy).optimize(query, dataset, bindings);		// maybe remove this optimizer later

// System.out.println("query after ConstantOptimizer: " + query);

		new DisjunctiveConstraintOptimizer().optimize(query, dataset, bindings);


		/*
		 * TODO
		 * add some generic optimizers:
		 *  - FILTER ?s=1 && ?s=2 => EmptyResult
		 *  - Remove variables that are not occuring in query stmts from filters
		 */


		/* custom optimizers, execute only when needed*/

		GenericInfoOptimizer info = new GenericInfoOptimizer(queryInfo);
		statementPatterns = info.getStatements();
		// collect information and perform generic optimizations
// System.out.println("Query before info.optimize: " + query);
		info.optimize(query); // Replaces 'Join' operations with 'NJoin' of FedX
// System.out.println("Query after info.optimize: " + query);
		// Source Selection: all nodes are annotated with their source
		long srcSelTimeStrt = System.currentTimeMillis();

// if(info.hasUnion())
// 	System.out.println("HAS Union:");

		SourceSelection sourceSelection = new SourceSelection(members, cache, queryInfo);
		sourceSelection.doSourceSelection(info.getStatements()); // adds 'ExclusiveStatement', 'StatementSourcePattern', and the corresponding endpoints
// System.out.println("Query after SS: " + query);
		long srcSelTime = System.currentTimeMillis()-srcSelTimeStrt;

		// if the query has a single relevant source (and if it is not a SERVICE query), evaluate at this source only
		Set<Endpoint> relevantSources = sourceSelection.getRelevantSources();


		if (info.hasService())
			new ServiceOptimizer(queryInfo).optimize(query);

		// optimize Filters, if available
		if (info.hasFilter())
			new FilterOptimizer().optimize(query);

		// optimize unions, if available
  	if (info.hasUnion())
			new UnionOptimizer(queryInfo).optimize(query);

long startTime = System.currentTimeMillis();
		new BgpQueryPlanner(queryInfo).getQuery(query); // Decomposes query for Grafeque BGP optimization
// System.out.println("BgpQueryPlanner time: " + (System.currentTimeMillis()-startTime));

		return query;
	}

}
