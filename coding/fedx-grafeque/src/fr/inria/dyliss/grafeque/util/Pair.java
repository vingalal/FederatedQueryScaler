/*
 * @author Vijay Ingalalli
 */


package fr.inria.dyliss.grafeque.util;

import java.io.Serializable;

public class Pair<X, Y> implements Serializable {
  public X first;
  public Y second;
  public Pair(X first, Y second) {
    this.first = first;
    this.second = second;
  }
}
