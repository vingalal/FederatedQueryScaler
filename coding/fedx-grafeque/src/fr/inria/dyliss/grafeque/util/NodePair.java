package fr.inria.dyliss.grafeque.util;

public class NodePair{ // Used exclusively for Map implementation (e.g.: edges), in order to have immutable object key for HashMap.
	private final int first;
	private final int second;
	public NodePair(Integer first, Integer second) {
		this.first = first;
		this.second = second;
	}

	public int getFirst() {
	 return first;
 }
	public int getSecond() {
	 return second;
 }
	// Following overrides are necessary to use Pair as a 'key' in a hashmap.
	@Override
	public boolean equals (final Object O) {
		if (!(O instanceof NodePair)) return false;
		if (((NodePair) O).first != first) return false;
		if (((NodePair) O).second != second) return false;
		return true;
	}
	@Override
	public int hashCode() {
		return (first << 16) + second;
	}

}
