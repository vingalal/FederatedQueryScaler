package fr.inria.dyliss.grafeque.util;

import java.util.Map;
import java.util.HashMap;
import java.util.BitSet;

public class RdfSummary{
  public Map<String, Integer> predicateMap = new HashMap<>();
  public BitSet[][][] kNeighbours; // = new BitSet[][][];
  public Integer[][][] kFrequencies; // = new Integer[][][];
}
