package fr.inria.dyliss.grafeque.util;

import java.util.Objects;


public class HashPair{

	private final String first;
	private final String second;
	public HashPair(String first, String second) {
		this.first = first;
		this.second = second;
	}

	public String getFirst() {
	 return first;
 	}
	public String getSecond() {
	 return second;
 	}

	// Following overrides are necessary to use Pair as a 'key' in a hashmap.
	@Override
	public boolean equals (final Object O) {
		if (!(O instanceof HashPair)) return false;
		if (((HashPair) O).first != first) return false;
		if (((HashPair) O).second != second) return false;
		return true;
	}
	// @Override public int hashCode() {
	//     return Objects.hash(first, second);
	// }
  @Override
  public int hashCode()
  {
      final int prime = 31;
      int result = 1;
      result = prime * result + (first ==null? 0 : first.hashCode());
      result = prime * result + (second ==null? 0 : second.hashCode());
      return result;
  }
}
