package fr.inria.dyliss.grafeque.util;

import java.util.ArrayList;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.HashSet;
import fr.inria.dyliss.grafeque.util.NodePair;


public class GraphProperty{
	public ArrayList<Set<Integer>> adjList = new ArrayList<>();
	public Map<NodePair, ArrayList<Integer>> edges = new LinkedHashMap<NodePair, ArrayList<Integer>>();
	public Set<Integer> uniqueEdges = new HashSet<Integer>();
	public Set<ArrayList<Integer>> uniqueMultiedges = new HashSet<ArrayList<Integer>>();
}
