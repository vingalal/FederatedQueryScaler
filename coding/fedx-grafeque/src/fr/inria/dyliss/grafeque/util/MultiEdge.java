/*
 * @author Vijay Ingalalli
 */


package fr.inria.dyliss.grafeque.util;

import java.util.ArrayList;
import fr.inria.dyliss.grafeque.util.Pair;

public class MultiEdge<X, Y, ArrayList > extends Pair<X, Y> {
	public final ArrayList mlbl;
	public MultiEdge(X srcN, Y objN, ArrayList mlbl){
		super(srcN, objN);
		this.mlbl = mlbl;
	}
}
