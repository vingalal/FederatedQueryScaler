/*
 * Copyright (C) 2008-2013, fluid Operations AG
 *
 * FedX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.fluidops.fedx.optimizer;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.aksw.simba.start.QueryEvaluation;
import org.apache.log4j.Logger;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.algebra.QueryRoot;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.evaluation.impl.ConstantOptimizer;
import org.openrdf.query.algebra.evaluation.impl.DisjunctiveConstraintOptimizer;
import org.openrdf.query.algebra.evaluation.impl.EvaluationStrategyImpl;
import org.openrdf.sail.SailException;

import com.fluidops.fedx.FedX;
import com.fluidops.fedx.FederationManager;
import com.fluidops.fedx.algebra.SingleSourceQuery;
import com.fluidops.fedx.cache.Cache;
import com.fluidops.fedx.structures.Endpoint;
import com.fluidops.fedx.structures.QueryInfo;

import fr.inria.dyliss.grafeque.graph.BgpQueryPlanner;
import fr.inria.dyliss.grafeque.planoptimizer.BgpOptimizer;

public class Optimizer {

	public static Logger logger = Logger.getLogger(Optimizer.class);


	public static TupleExpr optimize(TupleExpr parsed, Dataset dataset, BindingSet bindings,
			EvaluationStrategyImpl strategy, QueryInfo queryInfo) throws SailException {


		FedX fed = FederationManager.getInstance().getFederation();
		List<Endpoint> members = fed.getMembers();

		// if the federation has a single member only, evaluate the entire query there
		if (members.size()==1 && queryInfo.getQuery()!=null)
			return new SingleSourceQuery(parsed, members.get(0), queryInfo);

		// Clone the tuple expression to allow for more aggressive optimizations
		TupleExpr query = new QueryRoot(parsed.clone());

// System.out.println("query at the beginning: " + query);

		Cache cache = FederationManager.getInstance().getCache();

		//if (logger.isTraceEnabled())
		//	logger.trace("Query before Optimization: " + query);


		/* original sesame optimizers */
		new ConstantOptimizer(strategy).optimize(query, dataset, bindings);		// maybe remove this optimizer later

// System.out.println("query after ConstantOptimizer: " + query);

		new DisjunctiveConstraintOptimizer().optimize(query, dataset, bindings);


		/*
		 * TODO
		 * add some generic optimizers:
		 *  - FILTER ?s=1 && ?s=2 => EmptyResult
		 *  - Remove variables that are not occuring in query stmts from filters
		 */


		/* custom optimizers, execute only when needed*/

		GenericInfoOptimizer info = new GenericInfoOptimizer(queryInfo);

		// collect information and perform generic optimizations
		info.optimize(query); // Replaces 'Join' operations with 'NJoin' of FedX

TupleExpr originalQuery = new QueryRoot(parsed.clone());
info.optimize(originalQuery);

// System.out.println("query after generic optimizations and before SS: " + query);

		// Source Selection: all nodes are annotated with their source
		long srcSelTimeStrt = System.currentTimeMillis();

// System.out.println("query before SS: " + query);
// System.out.println("query info before SS: " + info.getStatements());

		SourceSelection sourceSelection = new SourceSelection(members, cache, queryInfo);
// System.out.println("queryInfo before SS: " + queryInfo.getQueryType());
		sourceSelection.doSourceSelection(info.getStatements()); // adds 'ExclusiveStatement' and the corresponding endpoints
// System.out.println("queryInfo after SS: " + queryInfo.getQueryType());
// System.out.println("query info after SS: " + info.getStatements());
// System.out.println("query after SS: " + originalQuery);
		long srcSelTime = System.currentTimeMillis()-srcSelTimeStrt;
		// System.out.println("Source selection time: "+ srcSelTime+"\n");

		// if the query has a single relevant source (and if it is not a SERVICE query), evaluate at this source only
		Set<Endpoint> relevantSources = sourceSelection.getRelevantSources();
		// System.out.println("Distinct Relevant Sources:" + relevantSources.size());
		// System.out.println("Total No. of. Sources???:" + relevantSources.size());

// System.out.println("query before StatementGroupOptimize: " + query);

		if (relevantSources.size()==1 && !info.hasService())
			return new SingleSourceQuery(query, relevantSources.iterator().next(), queryInfo);

		if (info.hasService())
			new ServiceOptimizer(queryInfo).optimize(query);

		// optimize Filters, if available
		if (info.hasFilter())
			new FilterOptimizer().optimize(query);

		// optimize unions, if available
		if (info.hasUnion)
			new UnionOptimizer(queryInfo).optimize(query);

		// optimize statement groups and join order
// System.out.println("query before StatementGroupOptimize: " + query);

		// new BgpQueryPlanner(queryInfo).getQuery(query); // Decomposes query for Grafeque BGP optimization
// System.out.println("originalQuery: " + originalQuery);
		new StatementGroupOptimizer(queryInfo).optimize(query); // adds 'ExclusiveGroup' and 'StatementSourcePattern'
		// // BgpOptimizer adds 'ExclusiveGroup' and retains 'StatementSourcePattern' as per grafeque query plan
		// new BgpOptimizer(queryInfo).optimize(originalQuery); // using 'origianl-query' that has only 'StatementPattern' and not 'ExclusiveStatement', 'StatementSourcePattern'.
		// new BgpOptimizer(queryInfo).optimize(query); // 'query' already has 'ExclusiveStatement', 'StatementSourcePattern'

// System.out.println("query after StatementGroupOptimize: " + query);
		new VariableScopeOptimizer(queryInfo).optimize(query); // adds 'LocalVars'

	//	if (logger.isTraceEnabled())
		//	logger.trace("Query after Optimization: " + query);

// System.out.println("query after Optimization: " + query);
// System.out.println("queryInfo after Optimization: " + queryInfo);

		return query;
	}

}
