#!/bin/bash

binFolder="/home/vijay/git/FederatedQueryScaler/coding/fedx-grafeque/bin/"
fedx="/home/vijay/git/FederatedQueryScaler/coding/fedx-grafeque/src/com/fluidops/fedx/"
grafeque="/home/vijay/git/FederatedQueryScaler/coding/fedx-grafeque/src/fr/inria/dyliss/grafeque/"
ppinss="/home/vijay/git/FederatedQueryScaler/coding/fedx-grafeque/src/fr/inria/dyliss/ppinss/"
resources="/home/vijay/git/FederatedQueryScaler/coding/resources/"
resFile="/home/vijay/git/FederatedQueryScaler/coding/fedx-grafeque/results/results.txt"


### Compile modified files
## grafeque
# javac -cp bin:lib/* $grafeque"util/RdfSummary.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"util/Pair.java" -d $binFolder
#javac -cp bin:lib/* $grafeque"util/MultiEdge.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"util/NodePair.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"util/HashPair.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"util/GraphProperty.java" -d $binFolder

# javac -cp bin:lib/* $grafeque"summary/SummaryReader.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"summary/SummaryWriter.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"summary/SummaryBuilder.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"summary/SummaryManager.java" -d $binFolder


# javac -cp bin:lib/* $grafeque"planoptimizer/BgpOptimizer.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"evaluation/ParallelUnionOperatorTask.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"evaluation/ParallelServiceExecutor.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"planoptimizer/GrafequeEvalStrategy.java" -d $binFolder
# javac -cp bin:lib/* $grafeque"planoptimizer/SourceSelector.java" -d $binFolder
# # javac -cp bin:lib/* $grafeque"graph/BgpQueryPlanner.java" -d $binFolder -Xlint:unchecked
# javac -cp bin:lib/* $grafeque"algebra/NUnionG.java" -d $binFolder

javac -cp bin:lib/* $grafeque"CLI.java" -d $binFolder

## ppinss
# javac -cp bin:lib/* $ppinss"util/PPinSSConfig.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"util/PPinSSLogger.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"util/PPinSSTimer.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"dbsum/DbSumGenerator.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"dbsum/AuthorityFactory.java" -d $binFolder

  ## not needed yet
# javac -cp bin:lib/* $ppinss"util/QueryInfoExtractor.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"util/QueryParser.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"util/QueryStatementsAnalyzer.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"dbsum/IndexManager.java" -d $binFolder
# javac -cp bin:lib/* $ppinss"dbsum/HibiscusFedSumGenerator.java" -d $binFolder


## fedx
## modified files.
# javac -cp bin:lib/* $fedx"CLI.java" -d $binFolder
# javac -cp bin:lib/* $fedx"optimizer/Optimizer.java" -d $binFolder
## not-yet modified files
javac -cp bin:lib/* $fedx"optimizer/SourceSelection.java" -d $binFolder
##javac -cp bin:lib/* $fedx"optimizer/StatementGroupOptimizer.java" -d $binFolder
##javac -cp bin:lib/* $fedx"optimizer/VariableScopeOptimizer.java" -d $binFolder
javac -cp bin:lib/* $fedx"FedXConnection.java" -d $binFolder
# javac -cp bin:lib/* $fedx"FederationManager.java" -d $binFolder
# javac -cp bin:lib/* $fedx"QueryManager.java" -d $binFolder

javac -cp bin:lib/* $grafeque"graph/GraphQueryManager.java" -d $binFolder
javac -cp bin:lib/* $grafeque"graph/BgpQueryPlanner.java" -d $binFolder
javac -cp bin:lib/* $grafeque"planoptimizer/Optimizer.java" -d $binFolder
javac -cp bin:lib/* $grafeque"evaluation/GrafequeEvaluator.java" -d $binFolder
javac -cp bin:lib/* $fedx"evaluation/FederationEvalStrategy.java" -d $binFolder


# using fedx-grafeque (old)
## java -Xmx1024m -cp bin:lib/* com.fluidops.fedx.CLI -c examples/config.prop -d examples/endpoints.ttl @q examples/q3.txt
## java -Xmx1024m -cp bin:lib/* com.fluidops.fedx.CLI -d examples/endpoints.ttl @s summaries/ @q examples/q3.txt


# summary building for grafeque
# java -Xmx15360m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -e $resources"endpoints.txt" @s $resources"summ/"

# using fedx-grafeque (new)
# java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d $resources"endpoints.ttl" @s $resources"summaries/" @q $resources"queries/q3.txt"
# java -Xmx4096m -cp bin:lib/* com.fluidops.fedx.CLI -d $resources"endpoints.ttl" @s $resources"summaries/" @q $resources"queries/q3.txt"
# java -Xmx4096m -cp bin:lib/* com.fluidops.fedx.CLI -d $resources"endpoints.ttl" @s $resources"summaries/" @q $resources"queries/fedbench/ls-3.txt"

# java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d $resources"endpoints.ttl" -f "STDOUT" @q $resources"benchmarking/fedBench/queries/all/cd7"
java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d $resources"endpoints.ttl"  -f "STDOUT" @s $resources"summaries/" @q $resources"benchmarking/fedBench/queries/all/ls2"
# java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d $resources"endpoints.ttl" @s $resources"summaries/" @q $resources"benchmarking/largeRdfBench/queries/all/ch3"

<<COMMENT

declare -a queries=("cd1" "cd2" "cd3" "cd4" "cd5" "cd6" "cd7" "ld1" "ld2" "ld3" "ld4" "ld5" "ld6" "ld7" "ld8" "ld9" "ld10" "ld11" "ls1" "ls2" "ls3" "ls4" "ls5" "ls6" "ls7")
# declare -a queries=("cd1" "cd2" "cd3")

touch $resFile
for qry in "${queries[@]}"
do
  printf $qry"\n" >> $resFile
  printf $qry", "
  # java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d $resources"endpoints.ttl" -f "STDOUT" @q $resources"benchmarking/fedBench/queries/all/"$qry >> $resFile
  java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d $resources"endpoints.ttl"  -f "STDOUT" @s $resources"summaries/" @q $resources"benchmarking/fedBench/queries/all/"$qry >> $resFile
  printf "\n" >> $resFile
done
printf "\n"


COMMENT

<<COMMENT
To run FedX,
0. Run the command above:
    java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d $resources"endpoints.ttl" -f "STDOUT" @q $resources"benchmarking/fedBench/queries/all/"$qry >> $resFile
1. In CLI.java, comment the line:
    parseSummaries(args)
2. In FedXConnection, replace:
    * import fr.inria.dyliss.grafeque.planoptimizer.Optimizer by import com.fluidops.fedx.optimizer.Optimizer
    * return strategy.evaluateG(query, EmptyBindingSet.getInstance()) by return strategy.evaluate(query, EmptyBindingSet.getInstance());
COMMENT
