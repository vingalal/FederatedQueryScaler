package grafeque;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

class TimeEvaluation{
	static double time1 = 0;
	static double time2 = 0;
	static double time3 = 0;
	static double time4 = 0;	
}

class Edge extends Pair<Integer, Integer>{
	public final int lbl;	
	public Edge(int srcN, int objN, int lbl){
		super(srcN, objN);
		this.lbl = lbl;
	}
}


class Multiedge extends Pair<Integer, Integer> {
	public final ArrayList<Integer> mlbl;
	public final boolean sign;
	public Multiedge(int srcN, int objN, ArrayList<Integer> mlbl, boolean sign){
		super(srcN, objN);
		this.mlbl = mlbl;
		this.sign = sign;
	}
}


class GraphOperations{
	
	public static ArrayList<Set<Integer>> makeUnion(ArrayList<Set<Integer>> kNeighboursOld, ArrayList<Set<Integer>> kNeighboursNew){
		int minSize = Math.min(kNeighboursOld.size(), kNeighboursNew.size());
		ArrayList<Set<Integer>> result = new ArrayList<Set<Integer>>();
		if(kNeighboursOld.size() >= kNeighboursNew.size())
			result.addAll(kNeighboursOld);
		else
			result.addAll(kNeighboursNew);
		for(int k = 0; k < minSize; ++k){ // Make union for all k-neighbours
			if(kNeighboursOld.size() >= kNeighboursNew.size()){
				result.get(k).addAll(kNeighboursNew.get(k));
//				result.get(k).second.addAll(kNeighboursNew.get(k).second);
			}
			else{
				result.get(k).addAll(kNeighboursOld.get(k));
//				result.get(k).second.addAll(kNeighboursOld.get(k).second);
			}
		}		
		return result;
	}
	
	public static ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> mergeMaps(ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> kAdjSummary, Map<Integer, Pair<BitSet, MutableInt>> adjSummary, int k){
		ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> kAdjSummaryNew = new ArrayList<>();
		int adjSize = kAdjSummary.size();
		// collect the unchangeable summary for values < k.
		for(int i = 0; i < k; ++i) 
			kAdjSummaryNew.add(kAdjSummary.get(i));

		// Merge the values for 'k'
		Map<Integer, Pair<BitSet, MutableInt>> adjSummaryNew = kAdjSummary.get(k);
		for(Map.Entry<Integer, Pair<BitSet, MutableInt>> entry : adjSummary.entrySet()) {
			int pred = entry.getKey();
			if(adjSummaryNew.containsKey(entry.getKey())) {
				for(int b = 0; b < 4; ++b)
					if(entry.getValue().first.get(b))
						adjSummaryNew.get(pred).first.set(1);
				adjSummaryNew.get(pred).second.increment();
			}
			else {
				adjSummaryNew.put(pred, entry.getValue());
			}
		}
		kAdjSummaryNew.add(adjSummaryNew); // kth modified/merged element 
		
		// collect the unchangeable summary for values >k.
		for(int i = k+1; i < adjSize; ++i) 
			kAdjSummaryNew.add(kAdjSummary.get(i));
		
		return kAdjSummary;
	}
	
	public static void getAdjList(GraphData graphData, int nNodes){
		graphData.adjList.ensureCapacity(nNodes);
    Set<Integer> adjNull = new HashSet<Integer>();
//    Collections.
    
    for(int i  = 0; i < nNodes; ++i)
    	graphData.adjList.add(adjNull);    
    
    for(Map.Entry<NodePair, ArrayList<Integer>> entry : graphData.edges.entrySet()){
  	// A node with incoming edge is positive, and a node with outgoing edge is negative.
  	int nodeS = entry.getKey().getFirst();
  	int nodeO = entry.getKey().getSecond();
  	if(graphData.adjList.get(nodeS).isEmpty()){ // adding a fresh adjacent node
    	Set<Integer> adj = new HashSet<Integer>();
    	adj.add(nodeO);
    	graphData.adjList.set(nodeS, adj);
  	}
  	else{ // at least one adjacent node already exists 
  		Set<Integer> adj = graphData.adjList.get(nodeS);
  		adj.add(nodeO);
  		graphData.adjList.set(nodeS, adj);
  	}
  	if(graphData.adjList.get(nodeO).isEmpty()){ // adding a fresh adjacent node
    	Set<Integer> adj = new HashSet<Integer>();
    	adj.add(nodeS + nNodes);
    	graphData.adjList.set(nodeO, adj);
  	}
  	else{ // at least one adjacent node already exists 
  		Set<Integer> adj = graphData.adjList.get(nodeO);
  		adj.add(nodeS + nNodes);
  		graphData.adjList.set(nodeO, adj);
  	}
  }
  
  /// Output adjacent list size for few vertices 
	Set<Integer> adjListSize = new TreeSet<Integer>();
	int avgDegree = 0;
	for(int i = 0; i < nNodes; ++ i){
		adjListSize.add(graphData.adjList.get(i).size());
		avgDegree+=graphData.adjList.get(i).size();
	}
	LinkedList<Integer> list = new LinkedList<>(adjListSize);
	Iterator<Integer> itr = list.descendingIterator();
	int count = 0;
	while(itr.hasNext()) {
		System.out.print(itr.next() + ", ");
		if (count > 10)
		break;
		++count;
	}
	System.out.println();
	System.out.println("Average node degree: " + (double)avgDegree/nNodes);
	}
}



public class SourceSelector{
	
	// Select TPW sources from either ASK/ predicate list
	

	// Depending on the selected sources, make a set of BGPs that need to be sent to corresponding endpoints
	
	
	// List the number of permutations that need to be processed
	
	
	// Apply the predicateMatrix index to prune the number of permutations
	
	
	// Apply the Predicate Graph to prune more number of permutations
	
	
	// Perform query planning using the frequency information from the indexes
	
	
	// Send the query for query execution
	
	
	
}
