package grafeque;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import grafeque.FileManager;

public class QueryManager {
	
	static Map<String, String> predicatesFull = new HashMap<String, String>(); // <truncatedPredicate, extendedPredicate>
//  static Map<NodePair, ArrayList<Integer>> queryEdges = new HashMap<NodePair, ArrayList<Integer>>();   
  static Map<String, Integer> nodeMap = new HashMap<String, Integer>();
  static Map<String, Integer> edgeMap = new HashMap<String, Integer>();

  private static String removeLastChar(String str) {
    return str.substring(0, str.length() - 1);
  }
  
  private static void getPredicateMapping(Map<String, String> predicates){
  	int p = 0;
  	for(Map.Entry<String, String> entry : predicates.entrySet()){
  		edgeMap.put(entry.getValue(), p);
  		++p;
  	}  	
  }
  
	private static void getFullPredicates(){
		List<String> predicateStrings = FileManager.getPredicates();
		Map<String, String> prefixes = FileManager.getPrefixes();

		if(prefixes.isEmpty()){
			for(int i = 0; i < predicateStrings.size(); ++i)
				predicatesFull.put(predicateStrings.get(i), predicateStrings.get(i));
			getPredicateMapping(predicatesFull);			
			return;
		}
		
		for(int i = 0; i < predicateStrings.size(); ++i){
			if(predicateStrings.get(i).equals("a")){
				predicatesFull.put("a", "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>");
				continue;
			}
			try{
				String[] predicateParts = predicateStrings.get(i).split(":");
				String extendedPredicate = removeLastChar(prefixes.get(predicateParts[0] + ":")) + predicateParts[1] + ">";
				predicatesFull.put(predicateStrings.get(i), extendedPredicate);
			}
			catch(NullPointerException e){
				System.out.println("Prefix not found!");
			}			
		}
		getPredicateMapping(predicatesFull);
	}
	
	private static void createSparqlGraph(GraphData sparqlGraph){
		List<List<String>> tripleStrings = FileManager.getTriples();
	  int node = 0, nodeS, nodeO;
//		Map<String, Integer> predicateMap = FileManager.getPredicateMap();

		for(int i = 0; i < tripleStrings.size(); ++i){
			if(!nodeMap.containsKey(tripleStrings.get(i).get(0))){
				nodeMap.put(tripleStrings.get(i).get(0), node);
				nodeS = node;
				++node;
			}
			else
				nodeS = nodeMap.get(tripleStrings.get(i).get(0));
			if(!nodeMap.containsKey(tripleStrings.get(i).get(2))){
				nodeMap.put(tripleStrings.get(i).get(2), node);
				nodeO = node;
				++node;
			}
			else
				nodeO = nodeMap.get(tripleStrings.get(i).get(2));
			
			NodePair qNodes = new NodePair(nodeS, nodeO);
			ArrayList<Integer> qEdges = new ArrayList<Integer>();
			
			
			String predicate = predicatesFull.get(tripleStrings.get(i).get(1));
			if(sparqlGraph.edges.containsKey(qNodes)){ // a multiedge exists
				ArrayList<Integer> qEdgesUpdated = sparqlGraph.edges.get(qNodes);
				qEdgesUpdated.add(edgeMap.get(predicate));
				sparqlGraph.edges.replace(qNodes, qEdgesUpdated);
			}
			else{
				qEdges.add(edgeMap.get(predicate));
				sparqlGraph.edges.put(qNodes, qEdges);
			}
		}
		
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : sparqlGraph.edges.entrySet()){
			if(entry.getValue().size() == 1)
				sparqlGraph.uniqueEdges.add(entry.getValue().get(0));
			else
				sparqlGraph.uniqueMultiedges.add(entry.getValue());
		}
		
		GraphOperations.getAdjList(sparqlGraph, nodeMap.size());
		
	}
	
	public static void parseSparql(){

		Map<Integer, ArrayList<Set<Integer>>> sparqlSummary = new HashMap<Integer, ArrayList<Set<Integer>>>();
		
		getFullPredicates();
System.out.println("Preds: " + predicatesFull.size());
for(Map.Entry<String, String> entry : predicatesFull.entrySet())
System.out.println(entry.getKey() + " = " + entry.getValue());

		GraphData sparqlGraph = GraphData.initialize();

		createSparqlGraph(sparqlGraph);
	
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : sparqlGraph.edges.entrySet()){
    	int nodeS = entry.getKey().getFirst();
    	int nodeO = entry.getKey().getSecond();
System.out.println("Edge: " + nodeS + " " + nodeO + ": " + entry.getValue().get(0));			

    	Set<Integer> inputNodes = new HashSet<Integer>(Arrays.asList(nodeS+nodeMap.size(), nodeO));
    	Set<Integer> alreadyVisitedNodes = new HashSet<Integer>(Arrays.asList(nodeS, nodeO, nodeS+nodeMap.size())); // This is true only in the beginning.
  		ArrayList<Set<Integer>> kNeighbours = new ArrayList<Set<Integer>>();
  		while(!inputNodes.isEmpty()){	
				Set<Integer> allAdjNodes = new HashSet<Integer>();
				PredicateSummary.findKthNeighbours(inputNodes, alreadyVisitedNodes, allAdjNodes, kNeighbours, sparqlGraph); 		
    		alreadyVisitedNodes.addAll(inputNodes);
    		inputNodes.clear();
    		inputNodes.addAll(allAdjNodes);
    	}
  		System.out.println("kNeighbours size: " + kNeighbours.size());
  		
			for(int pred = 0; pred < entry.getValue().size(); ++pred){
				if(sparqlSummary.containsKey(entry.getValue().get(pred)))
					sparqlSummary.replace(entry.getValue().get(pred), GraphOperations.makeUnion(sparqlSummary.get(entry.getValue().get(pred)), kNeighbours));
				else
					sparqlSummary.put(entry.getValue().get(pred), kNeighbours);
			}			
		}	
		System.out.println("SPARQL Summary size: " + sparqlSummary.size());
		for(Map.Entry<Integer, ArrayList<Set<Integer>>> summary : sparqlSummary.entrySet()){
			System.out.print(summary.getKey() + ": ");
			for(int k = 0; k < summary.getValue().size(); ++k){
				System.out.print("(" + (k+1) + ")[");
				for(Integer subject : summary.getValue().get(k))
					System.out.print(subject + ",");
				System.out.print("] ");    				
			}
			System.out.println();
		}
		
	}
}
