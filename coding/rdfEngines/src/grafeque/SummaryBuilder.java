package grafeque;
import java.io.BufferedReader;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import grafeque.FileManager;
import java.io.FileReader;
import java.io.PrintWriter;

import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;

class MyStack2D<T> {
//	private int maxRow;
	private ArrayList<ArrayList<T>> stackArray;
//	private int row;
	private int[] pointer;
	
	public MyStack2D(int stackSize) {
//		ArrayList<ArrayList<T>> stackArray = new ArrayList<ArrayList<T>>(maxRow);
		stackArray = new ArrayList<ArrayList<T>>(stackSize);
		pointer = new int[stackSize];
		ArrayList<T> emptyElement = new ArrayList<T>();
//		row = -1;
		for(int i = 0 ; i < stackSize; ++i){
			stackArray.add(emptyElement); // initialize with empty ArrayList<T>
			pointer[i] = -1;
		}
	}
	public void push(ArrayList<T> stackElement, int r) { // An array is pushed onto stack
		stackArray.set(r, stackElement);
		pointer[r] = stackElement.size()-1;
	}
	public T pop(int r) { // An element from 'r'th row from stackArray is popped
//		System.out.println("pop-depth: " + r + " : " + stackArray.get(r).size() + "::" + column[r]); 
	  return stackArray.get(r).get(pointer[r]--);
	}
	public T peek(int r) {
	  return stackArray.get(r).get(pointer[r]);
	}
	public boolean isEmpty(int r) {
		return (pointer[r] == -1);
	}
	// Functions for debugging
	public int getSize(int r) {
	  return stackArray.get(r).size();
	}
	public T getEdges(int r, int i) {
	  return stackArray.get(r).get(i);
	}
}

class MutableInt {
  int value = 1; // initialize count =1
  public void increment() { ++value; }
  public int get() { return value; }
}

class MEdge<X, Y, ArrayList > extends Pair<X, Y> {
	public final ArrayList mlbl;
	public MEdge(X srcN, Y objN, ArrayList mlbl){
		super(srcN, objN);
		this.mlbl = mlbl;
	}
}

class PredicateMatrix {
	
	private static Map<Integer, ArrayList<Map<Integer, Pair<BitSet, MutableInt>>>> matrixSummary = new HashMap<>();

	private static void updateMatrixSummary(int k, ArrayList<Integer> inPredicates, Map<Integer, Pair<BitSet, MutableInt>> adjSummary) {
		
//		inPredicates.remove
		
		for(int p = 0; p < inPredicates.size(); ++p){
			int pred = inPredicates.get(p);
			if(!matrixSummary.containsKey(pred)) {
				ArrayList<Map<Integer, Pair<BitSet, MutableInt>>> adjPredicateSummary = new ArrayList<>();
				adjPredicateSummary.add(adjSummary);
				matrixSummary.put(pred, adjPredicateSummary);

			}
			else { // predicate already visited
				if(matrixSummary.get(pred).size() <= k) { // adj predicates for k=0 exist for sure; however k>=1 needs to be checked
					matrixSummary.get(pred).add(adjSummary);					
				}
				else { // adj predicate already exist for any value of k. Thus, Union operation is to be performed.
					matrixSummary.replace(pred, GraphOperations.mergeMaps(matrixSummary.get(pred), adjSummary, k));
				}
			}
		}
		
	}
	
	private static void updateAdjSummary(Map<Integer, Pair<BitSet, MutableInt>> adjSummary, int adjPredicate, int join) {		
		
		if(adjSummary.containsKey(adjPredicate)) {
			adjSummary.get(adjPredicate).first.set(join);
			adjSummary.get(adjPredicate).second.increment();
		}
		else {
			BitSet joins = new BitSet(4);
			joins.set(join);
			Pair<BitSet, MutableInt> newAdj = new Pair<BitSet, MutableInt>(joins, new MutableInt());
			adjSummary.put(adjPredicate, newAdj);
		}
	}
	
	public static void getAdjPredicates(Set<Integer> adjList, int node, GraphData rdfGraph, Map<Integer, Pair<BitSet, MutableInt>> adjSummary){
		int nNodes = rdfGraph.adjList.size();
		/*
		 * Convention on the join operations that happens among a pair of predicates (input predicate, corresponding adjacent predicate)
		 * input-P	adjacent-P
		 * object		subject	=	0
		 * subject	subject	=	1
		 * object		object	= 2
		 * subject	object	=	3		 
		 * */
//		adjSummary.get
		for(Integer adjNode : adjList){
			if(adjNode < nNodes){ // 'adjNode' is Object and 'node' may be Subject/Object. => you are loosing a predicate, and hence '-predicate'
				try{										
//System.out.println(node + "," + adjNode);
//System.out.println("1. Predicate Exists!");					
					if(node < nNodes){ // 'node' is an object
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(node, adjNode); 
						NodePair nP = new NodePair(node, adjNode);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 0); // when Source and Object are considered : second
//							updateAdjSummary(adjSummary, predicates.get(p) + rdfGraph.uniqueEdges.size(), 0); // when Source and Object are considered : second
						}
					}
					else{ // 'node' is a subject
//System.out.println(node + "," + adjNode);						
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(node-nNodes, adjNode); 
						NodePair nP = new NodePair(node-nNodes, adjNode);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 1); // when Source and Object are considered : second
//							updateAdjSummary(adjSummary, predicates.get(p) + rdfGraph.uniqueEdges.size(), 1); // when Source and Object are considered : second	
						}
					}
				} 
				catch(NullPointerException e) { // This exception should never be thrown
//System.out.println(node + "," + adjNode);
					System.out.println("1. Predicate does not exist in RDF");
				}				
			}
			else{ // 'adjNode' is Subject and 'node' may be Subject/Object => you are gaining a predicate, and hence '+predicate'
				try{
//System.out.println(adjNode + "," + node);	
//System.out.println("2. Predicate Exists!");					
					if(node < nNodes){ // 'node' is an object
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(adjNode-nNodes, node);
						NodePair nP = new NodePair(adjNode-nNodes, node);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 2); // when Source and Object are considered : second
						}
					}
					else{ // 'node' is a subject
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(adjNode-nNodes, node-nNodes); 
						NodePair nP = new NodePair(adjNode-nNodes, node-nNodes);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p) {
							updateAdjSummary(adjSummary, predicates.get(p), 3);
						}
					}
				}
				catch(NullPointerException e) { // This exception should never be thrown
//System.out.println(adjNode-nNodes + "," + node);					
					System.out.println("2. Predicate does not exist in RDF");
				}				
			}
		}
	}
	
	public static void findKthNeighbours(Set<Integer> inputNodes, Set<Integer> alreadyVisitedNodes, Set<Integer> allAdjNodes, ArrayList<Integer> inPredicates, int k, GraphData rdfGraph){
		 
		Map<Integer, Pair<BitSet, MutableInt>> adjSummary = new HashMap<Integer, Pair<BitSet, MutableInt>>(); // key = adj predicate; value = <join order, frequency>
		
		
		int nNodes = rdfGraph.adjList.size();
		for(Integer node : inputNodes){
			Set<Integer> adjNodes = new HashSet<Integer>();
			double startTime = System.currentTimeMillis();

			if(node < nNodes)
				adjNodes.addAll(rdfGraph.adjList.get(node));
			else
				adjNodes.addAll(rdfGraph.adjList.get(node-nNodes));

			Set<Integer> alreadyVisitedNodesDir = new HashSet<Integer>();
			for(Integer i : alreadyVisitedNodes){
				if(i < nNodes)
					alreadyVisitedNodesDir.add(i+nNodes);
				else
					alreadyVisitedNodesDir.add(i-nNodes);
			}
			adjNodes.removeAll(alreadyVisitedNodesDir);
			adjNodes.remove(node); // self loops are excluded					
			adjNodes.removeAll(alreadyVisitedNodes);
  		TimeEvaluation.time4+= System.currentTimeMillis()-startTime;			

			startTime = System.currentTimeMillis();
			getAdjPredicates(adjNodes, node, rdfGraph, adjSummary);
  		TimeEvaluation.time3+= System.currentTimeMillis()-startTime;
			allAdjNodes.addAll(adjNodes);
		}
		updateMatrixSummary(k, inPredicates, adjSummary);

//System.out.println(allAdjNodes.size());		
		/*
		 * When the graph has triangles, the adjacent vertices of 'inputNodes' may consist of 'inputNodesDir'.
		 * These opposite directed nodes have to be removed for the updated 'inputNodes' that are used for next iteration.
		 * Note that 'inputNodesDir' have to be removed only after running 'getAdjPredicates'; otherwise we loose a predicate in the triangle.
		 * Any other possible repeated nodes are automatically removed by maintaining 'alreadyVisitedNodes'.
		 * */
		if(allAdjNodes.isEmpty())
			return;
		
		Set<Integer> inputNodesDir = new HashSet<Integer>();
		for(Integer node : inputNodes){
			if(node < nNodes)
				inputNodesDir.add(nNodes + node);
			else
				inputNodesDir.add(node - nNodes);	
		}
		allAdjNodes.removeAll(inputNodesDir);				
	}
			
	protected static void buildPredicateSummary(int K, GraphData rdfGraph, String summaryPath){
	
		int count = 0;
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : rdfGraph.edges.entrySet()){
    	int nodeS = entry.getKey().getFirst();
    	int nodeO = entry.getKey().getSecond();

    	Set<Integer> inputNodes = new HashSet<Integer>(Arrays.asList(nodeS+rdfGraph.adjList.size(), nodeO));
    	Set<Integer> alreadyVisitedNodes = new HashSet<Integer>(Arrays.asList(nodeS, nodeO, nodeS+rdfGraph.adjList.size())); // This is true only in the beginning.

  	  		
  		int k = 0;
    	while(k < K && !inputNodes.isEmpty()){ 
				Set<Integer> allAdjNodes = new HashSet<Integer>();
				double startTime = System.currentTimeMillis();
    		findKthNeighbours(inputNodes, alreadyVisitedNodes, allAdjNodes, entry.getValue(), k, rdfGraph);
    		TimeEvaluation.time1+= System.currentTimeMillis()-startTime;  		
    		alreadyVisitedNodes.addAll(inputNodes);
    		inputNodes.clear();
    		inputNodes.addAll(allAdjNodes);
    		++k;
    	}
			
			++count;
			if((count % 100000) == 0){
			System.out.println(count);
			//    	break;
}
		}				
		int nEdges = rdfGraph.uniqueEdges.size();
		FileManager.printMatrixSummary(matrixSummary, nEdges, K, summaryPath);
		
		System.out.println("Summary size: " + matrixSummary.size());		
		for(Map.Entry<Integer, ArrayList<Map<Integer, Pair<BitSet, MutableInt>>>> summary : matrixSummary.entrySet()) {
			System.out.print(summary.getKey() + ": ");
			for(int k = 0; k < K; ++k) {
				for(Map.Entry<Integer, Pair<BitSet, MutableInt>> sA : summary.getValue().get(k).entrySet()) {
					System.out.print(sA.getKey() + ":" + sA.getValue().first + "," + sA.getValue().second.get() + "// ");
				}				
				System.out.print("\t");
			}
			System.out.println();
		}
		
		System.out.println("Partial Time: " + TimeEvaluation.time1/1000 + " : " + TimeEvaluation.time3/1000 + " : " + TimeEvaluation.time4/1000 + " : " + TimeEvaluation.time2/1000);
	}
	
}

//********************************************************************************//


class PredicateSummary{	
	
	public static void getAdjPredicates(Set<Integer> adjList, Set<Integer> kthNeighbour, int node, GraphData rdfGraph){
		int nNodes = rdfGraph.adjList.size();
		
		for(Integer adjNode : adjList){
			if(adjNode < nNodes){ // 'adjNode' is Object and 'node' may be Subject/Object. => you are loosing a predicate, and hence '-predicate'
				try{										
//System.out.println(node + "," + adjNode);
//System.out.println("1. Predicate Exists!");					
					if(node < nNodes){ // 'node' is an object
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(node, adjNode); 
						NodePair nP = new NodePair(node, adjNode);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p)
							kthNeighbour.add(predicates.get(p) + rdfGraph.uniqueEdges.size()); // when Source and Object are considered : second					
					}
					else{ // 'node' is a subject
//System.out.println(node + "," + adjNode);						
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(node-nNodes, adjNode); 
						NodePair nP = new NodePair(node-nNodes, adjNode);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p)
							kthNeighbour.add(predicates.get(p) + rdfGraph.uniqueEdges.size()); // when Source and Object are considered : second						
					}
				} 
				catch(NullPointerException e) { // This exception should never be thrown
//System.out.println(node + "," + adjNode);
					System.out.println("1. Predicate does not exist in RDF");
				}				
			}
			else{ // 'adjNode' is Subject and 'node' may be Subject/Object => you are gaining a predicate, and hence '+predicate'
				try{
//System.out.println(adjNode + "," + node);	
//System.out.println("2. Predicate Exists!");					
					if(node < nNodes){ // 'node' is an object
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(adjNode-nNodes, node);
						NodePair nP = new NodePair(adjNode-nNodes, node);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p)
							kthNeighbour.add(predicates.get(p));  // when Source and Object are considered : second						
					}
					else{ // 'node' is a subject
//						Pair<Integer, Integer> nP = new Pair<Integer, Integer>(adjNode-nNodes, node-nNodes); 
						NodePair nP = new NodePair(adjNode-nNodes, node-nNodes);
						ArrayList<Integer> predicates = new ArrayList<Integer>();
						predicates.addAll(rdfGraph.edges.get(nP));
						for(int p = 0; p < predicates.size(); ++p)
							kthNeighbour.add(predicates.get(p)); 													
					}
				}
				catch(NullPointerException e) { // This exception should never be thrown
//System.out.println(adjNode-nNodes + "," + node);					
					System.out.println("2. Predicate does not exist in RDF");
				}				
			}
		}
	}
	
	public static void findKthNeighbours(Set<Integer> inputNodes, Set<Integer> alreadyVisitedNodes, Set<Integer> allAdjNodes, ArrayList<Set<Integer>> kNeighbours, GraphData rdfGraph){
		int nNodes = rdfGraph.adjList.size();
		Set<Integer> kthNeighbours = new HashSet<Integer>();
		for(Integer node : inputNodes){
			Set<Integer> adjNodes = new HashSet<Integer>();
			double startTime = System.currentTimeMillis();

			if(node < nNodes)
				adjNodes.addAll(rdfGraph.adjList.get(node));
			else
				adjNodes.addAll(rdfGraph.adjList.get(node-nNodes));

			Set<Integer> alreadyVisitedNodesDir = new HashSet<Integer>();
			for(Integer i : alreadyVisitedNodes){
				if(i < nNodes)
					alreadyVisitedNodesDir.add(i+nNodes);
				else
					alreadyVisitedNodesDir.add(i-nNodes);
			}
			adjNodes.removeAll(alreadyVisitedNodesDir);
			adjNodes.remove(node); // self loops are excluded					
			adjNodes.removeAll(alreadyVisitedNodes);
  		TimeEvaluation.time4+= System.currentTimeMillis()-startTime;			

			startTime = System.currentTimeMillis();
			getAdjPredicates(adjNodes, kthNeighbours, node, rdfGraph);
  		TimeEvaluation.time3+= System.currentTimeMillis()-startTime;
			allAdjNodes.addAll(adjNodes);
		}
//System.out.println(allAdjNodes.size());		
		/*
		 * When the graph has triangles, the adjacent vertices of 'inputNodes' may consist of 'inputNodesDir'.
		 * These opposite directed nodes have to be removed for the updated 'inputNodes' that are used for next iteration.
		 * Note that 'inputNodesDir' have to be removed only after running 'getAdjPredicates'; otherwise we loose a predicate in the triangle.
		 * Any other possible repeated nodes are automatically removed by maintaining 'alreadyVisitedNodes'.
		 * */
		if(allAdjNodes.isEmpty())
			return;
		
		Set<Integer> inputNodesDir = new HashSet<Integer>();
		for(Integer node : inputNodes){
			if(node < nNodes)
				inputNodesDir.add(nNodes + node);
			else
				inputNodesDir.add(node - nNodes);	
		}
		allAdjNodes.removeAll(inputNodesDir);
		
		kNeighbours.add(kthNeighbours);
	}
	
	protected static void buildPredicateSummary(int K, GraphData rdfGraph, String summaryPath){
						
//		ArrayList<ArrayList<Pair<Set<Integer>, Set<Integer>>>> rdfSummary = new ArrayList<ArrayList<Pair<Set<Integer>, Set<Integer>>>>(rdfGraph.uniqueEdges.size());
		Map<Integer, ArrayList<Set<Integer>>> rdfSummary = new HashMap<Integer, ArrayList<Set<Integer>>>();
//		ArrayList<Pair<Set<Integer>, Set<Integer>>> kNeighbours = new ArrayList<Pair<Set<Integer>, Set<Integer>>>(k);
		int count = 0;
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : rdfGraph.edges.entrySet()){
//    	ArrayList<Edge> inputEdges = new ArrayList<Edge>();
    	int nodeS = entry.getKey().getFirst();
    	int nodeO = entry.getKey().getSecond();
//System.out.println("Edge: " + nodeS + " " + nodeO + ": " + entry.getValue().get(0));			

    	Set<Integer> inputNodes = new HashSet<Integer>(Arrays.asList(nodeS+rdfGraph.adjList.size(), nodeO));
    	Set<Integer> alreadyVisitedNodes = new HashSet<Integer>(Arrays.asList(nodeS, nodeO, nodeS+rdfGraph.adjList.size())); // This is true only in the beginning.
//    	Tuple kNeighbours = new Tuple(neighbours);
  		ArrayList<Set<Integer>> kNeighbours = new ArrayList<Set<Integer>>(K);
  		int k = 0;
    	while(k < K && !inputNodes.isEmpty()){    		
//System.out.print("inp: ");
//for(Integer o : inputNodes)
//System.out.print(o + ", ");   
//System.out.println();    		
//System.out.print("vst: ");
//for(Integer o : alreadyVisitedNodes)
//System.out.print(o + ", ");       	
//System.out.println(); 
				Set<Integer> allAdjNodes = new HashSet<Integer>();
				double startTime = System.currentTimeMillis();
    		findKthNeighbours(inputNodes, alreadyVisitedNodes, allAdjNodes, kNeighbours, rdfGraph);
    		TimeEvaluation.time1+= System.currentTimeMillis()-startTime;  		
    		alreadyVisitedNodes.addAll(inputNodes);
    		inputNodes.clear();
    		inputNodes.addAll(allAdjNodes);
    		++k;
//System.out.print("adj: ");    		
//for(Integer o : allAdjNodes)
//System.out.print(o + ", ");       		
//System.out.println();
//System.out.println("-----------");
    	}
    	
//System.out.print(nodeS + "," + nodeO + " " + entry.getValue() + ": ");
//for(int m = 0; m < kNeighbours.size(); ++m){
//	System.out.print("(" + (m+1) + ")[");
//for(Integer subject : kNeighbours.get(m))
//	System.out.print(subject + ",");
//System.out.print("] ");    				
//}
//System.out.println();

    	
    	double startTime = System.currentTimeMillis();
			for(int pred = 0; pred < entry.getValue().size(); ++pred){
				if(rdfSummary.containsKey(entry.getValue().get(pred)))
					rdfSummary.replace(entry.getValue().get(pred), GraphOperations.makeUnion(rdfSummary.get(entry.getValue().get(pred)), kNeighbours));
				else
					rdfSummary.put(entry.getValue().get(pred), kNeighbours);
			}
			TimeEvaluation.time2+= System.currentTimeMillis()-startTime;
			
++count;
if((count % 100000) == 0){
System.out.println(count);
//    	break;
}
		}				
		int nEdges = rdfGraph.uniqueEdges.size();

		System.out.println("Summary size: " + rdfSummary.size());
		
		FileManager.printRdfSummary(rdfSummary, nEdges, K, summaryPath);
		
		
//		for(Map.Entry<Integer, ArrayList<Set<Integer>>> summary : rdfSummary.entrySet()){
//			System.out.print(summary.getKey() + ": ");
//			for(int k = 0; k < summary.getValue().size(); ++k){
//				System.out.print("(" + (k+1) + ")[");
//				for(Integer pred : summary.getValue().get(k))
//					System.out.print(pred + ",");
//				System.out.print("] ");    				
//			}
//			System.out.println();
//		}
		
		System.out.println("Partial Time: " + TimeEvaluation.time1/1000 + " : " + TimeEvaluation.time3/1000 + " : " + TimeEvaluation.time4/1000 + " : " + TimeEvaluation.time2/1000);
	}
	
}

class TrieNode implements Serializable {
  int content; // As an extension to handling multiedges, treat Integer as HashSet<Integeres>
  Pair<Integer, Integer> frequency = new Pair<Integer, Integer>(0,0); // <Incoming from Parent (positive), Outgoing to parent (negative)>
  boolean isEnd; 
  int count;  
  LinkedList<TrieNode> childList; 
 
    /* Constructor */
  public TrieNode(int p){
    childList = new LinkedList<TrieNode>();
    isEnd = false;
    content = p;
    count = 0;
    Pair<Integer, Integer> frequency = new Pair(0,0); // <Incoming from Parent (positive), Outgoing to parent (negative)>
  }  
  public TrieNode subNode(int p){
    if (childList != null)
      for (TrieNode eachChild : childList)
        if (eachChild.content == p)
          return eachChild;
    return null;
  }
}

 

class Trie implements Serializable {
  public TrieNode root; // keep it public only for debugging; else private
   /* Constructor */
  public Trie(){
    root = new TrieNode(-1); 
  }
  
  public void postOrder(TrieNode n){
  	TrieNode current = n;
    int i = 0;
//    System.out.println(i + ":" + current.content + ": " + current.childList.size()); // preOrder
    while(current.childList.size() != i){
    	postOrder(current.childList.get(i));
       i = i + 1;
    }
    System.out.print(current.content + ", "); // postOrder
  }
  
   /* Function to insert word */
  public void insert(ArrayList<Integer> word, ArrayList<Integer> direction){
    if (search(word) == true) 
      return;        
    TrieNode current = root; 
    for (int ch = 0; ch < word.size(); ++ch){
      TrieNode child = current.subNode(word.get(ch));
      if (child != null)
        current = child;
      else{
         current.childList.add(new TrieNode(word.get(ch)));
         current = current.subNode(word.get(ch));
      }
      current.count++;
      if(direction.get(ch) == 0)
      	current.frequency.first++; // a positive edge (this edge is coming from the parent)
      else
      	current.frequency.second++; //  a negative edge  (this edge is going towards its parent)
    }
    current.isEnd = true;
  }
  
  /* Function to search for word */
  public boolean search(ArrayList<Integer> word)
  {
    TrieNode current = root;  
    for (int ch : word){
      if (current.subNode(ch) == null)
        return false;
      else
        current = current.subNode(ch);
    }      
    if (current.isEnd == true) 
      return true;
    return false;
  }

  /* Function to remove a word */
  public void remove(ArrayList<Integer> word)
  {
    if (search(word) == false){
      System.out.println(word +" does not exist in trie\n");
    	return;
    }             
    TrieNode current = root;
    for (int ch : word){ 
      TrieNode child = current.subNode(ch);
      if (child.count == 1){
        current.childList.remove(child);
        return;
      } 
      else{
        child.count--;
        current = child;
      }
    }
    current.isEnd = false;
  }
}    


class TrieBuilder{

	private static void getAdjEdges(Set<Integer> adjNodes, int node, GraphData rdfGraph, ArrayList<Multiedge> adjEdges){
		int nNodes = rdfGraph.adjList.size();
		for(Integer adj : adjNodes){
			if(adj < nNodes){ // adj node is an object => adj edge is going away from parent. The child in Trie gets information. Hence set true
//				predicateSign.set(depth+1);
				NodePair adjPair = new NodePair(node, adj);				
//				Multiedge adjEdge = new Multiedge(node, adj, rdfGraph.edges.get(adjPair));
				Multiedge adjEdge = new Multiedge(-1, adj, rdfGraph.edges.get(adjPair), true);
				adjEdges.add(adjEdge);								
			}
			else{ // adj node is a subject => adj edge is going towards parent. The child in Trie looses information. Hence set false
				NodePair adjPair = new NodePair(adj-nNodes, node);				
//				Multiedge adjEdge = new Multiedge(adj-nNodes, node, rdfGraph.edges.get(adjPair));
				Multiedge adjEdge = new Multiedge(adj-nNodes, -1, rdfGraph.edges.get(adjPair), false);
				adjEdges.add(adjEdge);								
			}
		}
	}
	
	private static void getNewlyVisitedNodes(int node, int offset, HashSet<Integer> newlyVisitedNodes){
		newlyVisitedNodes.add(node);
		if(node < offset)
			newlyVisitedNodes.add(node+offset);
		else
			newlyVisitedNodes.add(node-offset);
	}
		
	public static void findNeighbouringPredicates(Multiedge inTriple, ArrayList<HashSet<Integer>> alreadyVisitedNodes, MyStack2D<Multiedge> myDfsStack, int depth, BitSet predicateSign , GraphData rdfGraph){
		
		int nNodes = rdfGraph.adjList.size();
		int nodeS = inTriple.first;
		int nodeO = inTriple.second;
		Set<Integer> adjNodesS = new HashSet<Integer>();
		Set<Integer> adjNodesO = new HashSet<Integer>();
		ArrayList<Multiedge> adjEdges = new ArrayList<Multiedge>();
		
		HashSet<Integer> newlyVisitedNodes = new HashSet<Integer>();
		if(nodeS != -1){
			getNewlyVisitedNodes(nodeS, nNodes, newlyVisitedNodes);
			adjNodesS.addAll(rdfGraph.adjList.get(nodeS));
			adjNodesS.removeAll(alreadyVisitedNodes.get(depth));
//System.out.println("adjNodesS: " + adjNodesS);	
			if(!adjNodesS.isEmpty()){
				getAdjEdges(adjNodesS, nodeS, rdfGraph, adjEdges);
				if(depth == 0)
					predicateSign.set(depth); // source of input has adjacent predicates. hence set to true.
			}
		}
		if(nodeO != -1){
//			predicateSign.set(depth); // object of input has adjacent predicates. hence NO need to set to true. default false value implies that object has adjacent vertices
			getNewlyVisitedNodes(nodeO, nNodes, newlyVisitedNodes);			
			adjNodesO.addAll(rdfGraph.adjList.get(nodeO));
			adjNodesO.removeAll(alreadyVisitedNodes.get(depth));
//System.out.println("adjNodesO: " + adjNodesO);
			if(!adjNodesO.isEmpty())
				getAdjEdges(adjNodesO, nodeO, rdfGraph, adjEdges);
			else{
				if(depth == 0)
					predicateSign.set(depth); // object of input has NO adjacent predicates. hence set to true. True value implies that object has no adj vertices.
			}
//for(int i = 0 ; i < adjEdges.size(); ++i)		
//System.out.print(adjEdges.get(i).mlbl + ", ");
//System.out.println();
		}

		myDfsStack.push(adjEdges, depth);		
		newlyVisitedNodes.addAll(alreadyVisitedNodes.get(depth)); // collect the already visited nodes from previous level
		alreadyVisitedNodes.set(depth+1, newlyVisitedNodes);
//System.out.println(predicateSign);
//System.out.println(alreadyVisitedNodes);
		
	}
	
	public static void updatePredicateTrie(ArrayList<ArrayList<Integer>> predicateWord, Trie predicateTrie, int predDepth){
			
		ArrayList<ArrayList<Integer>> word = new ArrayList<ArrayList<Integer>>();
//		word.addAll(predicateWord); //= predicateWord;
//		trimPredicateWord(word);		
		for(int s = 0; s < predDepth; ++s)
			word.add(s, predicateWord.get(s));
//		System.out.println(word);

		int K = word.size();
		ArrayList<Integer> predicateWordDirection = new ArrayList<Integer>(K+1);
		for(int i = 0; i < K; ++i)
			predicateWordDirection.add(0); // no directions set yet.
		
		for(Integer initPred : word.get(0)){
			ArrayList<Integer> uniquePredicateWord = new ArrayList<Integer>(K+1);
			for(int i = 0; i < K; ++i)
				uniquePredicateWord.add(-1);
			uniquePredicateWord.set(0, initPred);
			int depth = 1;
			MyStack2D<Integer> permutationDfsStack = new MyStack2D<Integer>(K);
			boolean fwd = true;
			
			while(depth != 0){
				if(depth == K){
					predicateTrie.insert(uniquePredicateWord, predicateWordDirection);
//System.out.println("unique: " + uniquePredicateWord);
					--depth;
					permutationDfsStack.pop(depth-1);
					fwd = false;
				}
				else{
					if(fwd){
//System.out.println(depth + ": " + predicateWord.size() + " :: " + predicateWord.get(depth));
						permutationDfsStack.push(word.get(depth), depth-1);
					}
					else
						permutationDfsStack.pop(depth-1);
				}
				if(!permutationDfsStack.isEmpty(depth-1)){
//System.out.println(depth + ": " + permutationDfsStack.peek(depth-1));
					uniquePredicateWord.set(depth, permutationDfsStack.peek(depth-1));
					++depth;
					fwd = true;
				}
				else{
					--depth;
					fwd = false;
				}
			}
		}		
	}
	
	public static void buildPredicateTrie(int K, GraphData rdfGraph, String summaryPath){ // Discovers query structure within an RDF store/endpoint
		
		Trie predicateTrie = new Trie();
		ArrayList<Integer> al = new ArrayList<Integer>();
		System.out.println(rdfGraph.edges.size());
		int count = 0;
		
		
		for(Map.Entry<NodePair, ArrayList<Integer>> entry : rdfGraph.edges.entrySet()){
			++count;
			if(count % 100 == 0)
				System.out.println(count);
    	int nodeS = entry.getKey().getFirst();
    	int nodeO = entry.getKey().getSecond();
//System.out.println("Edge: " + nodeS + " " + nodeO + ": " + entry.getValue());			

 		
			MyStack2D<Multiedge> myDfsStack = new MyStack2D<Multiedge>(K); // initial value is added only when neighbours are discovered, i.e., 1N. 
			int depth = 0;
			Multiedge initEdge = new Multiedge(nodeS, nodeO, entry.getValue(), false);
			ArrayList<Multiedge> initStackElement = new ArrayList<Multiedge>();
			initStackElement.add(initEdge);
			myDfsStack.push(initStackElement, depth); // push the edge at K=0
			ArrayList<ArrayList<Integer>> predicateWord = new ArrayList<ArrayList<Integer>>(K+1);
			BitSet predicateSign = new BitSet(K+1);
			
			ArrayList<HashSet<Integer>> alreadyVisitedNodes = new ArrayList<HashSet<Integer>>(K+1); 
			for(int i = 0; i <= K; ++i)
				alreadyVisitedNodes.add(null);
			HashSet<Integer> initVisitedNodes = new HashSet<Integer>(Arrays.asList(nodeS, nodeO, nodeS+rdfGraph.adjList.size())); // This is true only in the beginning.
			alreadyVisitedNodes.set(0, initVisitedNodes);

			for(int i = 0 ; i < K+1; ++i)
				predicateWord.add(al);
			predicateWord.set(0, entry.getValue());			
			depth++;
			Multiedge inEdge = initEdge;
//			HashSet<Integer> alreadyVisitedNodes = new HashSet<Integer>(Arrays.asList(nodeS, nodeO, nodeS+rdfGraph.adjList.size()));
			//System.out.println("initEdge: " + initEdge.first + "," + initEdge.second + " : " + initEdge.mlbl);

			boolean fwd = true;
      while(depth != 0 ){
        if(depth == K+1){
					updatePredicateTrie(predicateWord, predicateTrie, depth); // size K predicate word
//					System.out.println(predicateSign);					
					--depth;
					fwd = false;
//					break;					
        }
        else{
          if(fwd){
//System.out.println("Depth: " + (depth));       
//System.out.println("inEdge: " + inEdge.first + "," + inEdge.second + " : " + inEdge.mlbl);          	
          	findNeighbouringPredicates(inEdge, alreadyVisitedNodes, myDfsStack, depth-1, predicateSign, rdfGraph);
//System.out.println("Adj Size: " +  myDfsStack.getSize(depth-1));
//for(int i = 0; i < myDfsStack.getSize(depth-1); ++i)
//	System.out.print(myDfsStack.getEdges(depth-1, i).mlbl + ", ");
//System.out.println(); 
          }
        }
        if(!myDfsStack.isEmpty(depth-1)){
      		inEdge = myDfsStack.pop(depth-1);
//System.out.println("inEdge: " + inEdge.first + "," + inEdge.second + " : " + inEdge.mlbl);
      		predicateWord.set(depth, inEdge.mlbl);
      		if(inEdge.sign)
      			predicateSign.set(depth);
          ++depth;
          fwd = true;
        }
        else{
        	if(fwd){
        		updatePredicateTrie(predicateWord, predicateTrie, depth);  // predicate word with size < K
//System.out.println(predicateWord);
//System.out.println(alreadyVisitedNodes);
//System.out.println(predicateSign);
						predicateSign.clear();
        	}
          --depth;
          fwd = false;
        }
      }		
      if(count == 1000)
      	break;
		}
//    predicateTrie.postOrder(predicateTrie.root);
		FileManager.printPredicateTrie(predicateTrie, summaryPath);
	}
}

class PredicateGraph{
	public static ArrayList<String> collectMinimalResources(String endpoint){
		ArrayList<String> authorities = new ArrayList<String>();
		
		return authorities;
	}
}

public class SummaryBuilder {

	public static void buildSummary(int K, GraphData rdfGraph, String summaryPath) {
//		PredicateSummary.buildPredicateSummary(K, rdfGraph, summaryPath);
		PredicateMatrix.buildPredicateSummary(K, rdfGraph, summaryPath);
//		TrieBuilder.buildPredicateTrie(K, rdfGraph, summaryPath);
	}

	/*When no BGPs can be formed in a query, triples are sent separately to each endpoints.
	 *However, for a specific solution combination, we can test if the BGP 
	 *(that has a set of triples to be sent to the respective endpoints) exists in the predicateGraph.
	 *This avoids many local join operations.
	 *Build 'predicateGraph' for each endpoints 
	 *
	 * 1. The predicate graph can not be a multigraph since the subject-subject or object-object pair can't happen between 2 endpoints
	 * 2. Thus, a predicate graph is a directed simple graph where the possible joins are found as subject-object or object-subject. In the beginning it can be implemented as undirected graph.
	 * */
	
	public void readEpFile(String epPath, ArrayList<Pair<String, String>> endpoints)  throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(epPath));
		try{
			String line = br.readLine();
			while(line != null){
				if(line.trim().equals("")) // an empty line with/without spaces
					line = br.readLine();
				else {
					if(line.charAt(0) != '#'){ // not a comment
						String[] splitLine = line.split(" ");
						Pair<String, String> ep = new Pair(splitLine[0], splitLine[1]);
						endpoints.add(ep);
					}
					line = br.readLine();
				}
			}
		}
		finally {
			br.close();
		}
	}
	
	
	private static final HashMap<String, Integer> uriRepository = new HashMap<>();
	private Integer uriCount = 0;
	private static final ArrayList<ArrayList<Integer>> pgEdgeLabels = new ArrayList<>();
	
	private void createPGedgeLabels(){
		/*
		1: subject-subject
		2: subject-object
		3: object-subject
		4: object-object
		*/
		ArrayList<Integer> tmp = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(1,3));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(2,4));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(1,2));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(1));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(2));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(3,4));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(3));
		pgEdgeLabels.add(tmp);
		tmp = new ArrayList<>(Arrays.asList(4));
		pgEdgeLabels.add(tmp);
	}


	private List<String> retrievePredicates(RepositoryConnection con) throws RepositoryException, MalformedQueryException, QueryEvaluationException {
		String rQ = "SELECT (COUNT(?s) AS ?triples) WHERE { ?s ?p ?o }";
		TupleQuery qry = con.prepareTupleQuery(QueryLanguage.SPARQL, rQ);
		TupleQueryResult res = qry.evaluate();
		Long cnt = Long.parseLong(res.next().getValue("triples").stringValue());
		System.out.println("#Triples: " + cnt);

		String rawQuery = "SELECT distinct ?p WHERE { ?s ?p ?o }";
		TupleQuery query = con.prepareTupleQuery(QueryLanguage.SPARQL, rawQuery);
		TupleQueryResult result = query.evaluate();
		List<String> predicateSet = new ArrayList<String>();
		while (result.hasNext()) {
			BindingSet binds = result.next();
			Value predicate = binds.getValue("p");
			predicateSet.add(predicate.toString());
		}
		return predicateSet;
	}

	private void getEndpointResources(Pair<String, String> endpoint, HashMap<String, ArrayList<HashSet<Integer>>> resources, String summaryOutPath) throws RepositoryException, MalformedQueryException, QueryEvaluationException{
		// 0.0	Get all the predicates of an endpoint
		SPARQLRepository repo = new SPARQLRepository(endpoint.first);
		repo.initialize();
		RepositoryConnection conn = repo.getConnection();

		List<String> predicates = retrievePredicates(conn);
System.out.println("# Unique predicates: " + predicates.size());
// System.out.println(predicates);

	  SummaryWriter.writePredicateMappings(summaryOutPath + endpoint.second +  ".txt", predicates);


		//	1.1	Building predicate graph using HashMap implementation
		int usedPreds = 0;
		for(String predicate : predicates){
			if (predicate.contains("openlinksw.com"))
				continue;

			// HashSet<Integer> sub = new HashSet<>();
			// Get unique subject resources
			HashSet<Integer> subjects = new HashSet<>(); // Using HashSet and ArrayList has same time performance.
			String subjQuery = "SELECT DISTINCT ?s WHERE {?s <"+predicate+"> ?o. FILTER isURI(?s)}"; // DISTINCT drastically reduces time.  Not sure about FILTER yet
			TupleQueryResult subRes = conn.prepareTupleQuery(QueryLanguage.SPARQL, subjQuery).evaluate();
			while(subRes.hasNext()) {
				BindingSet binds = subRes.next();
				String subject = binds.getValue("s").stringValue();
				// TODO  Check the following line if there is a better way to fetch only the URIs from subject/object
				if(subject.startsWith("http") && !subject.contains("<www.w3.org/2001/XMLSchema#string>") &&  !subject.contains("localhost:8890/")){
					if(!uriRepository.containsKey(subject)){ // uri doesn't exist yet in the repository.
						uriRepository.put(subject, uriCount);
						++uriCount;
						subjects.add(uriCount); // a new subject uri
					}
					else
						subjects.add(uriRepository.get(subject)); // the current subject uri was already visited in some endpoint/predicate before.

				}
					// subjects.add(subject);
// if(subjects.size() >= 100000 && subjects.size()%100000 == 0)
// System.out.println("Subject resources: " + subjects.size());
			}
// System.out.println("Subject resources: " + subjects.size());

			// Get unique object resources
			HashSet<Integer> objects = new HashSet<>(); // Using HashSet and ArrayList has same time performance.
			HashSet<Integer> subjectsAndObjects = new HashSet<>();
			String objQuery = "SELECT DISTINCT ?o WHERE {?s <"+predicate+"> ?o. FILTER isURI(?o)}"; // DISTINCT drastically reduces time. Not sure about FILTER yet
			TupleQueryResult objRes = conn.prepareTupleQuery(QueryLanguage.SPARQL, objQuery).evaluate();
			while(objRes.hasNext()) {
				BindingSet binds = objRes.next();
				String object = binds.getValue("o").stringValue();
				// TODO  Check the following line if there is a better way to fetch only the URIs from subject/object
				if(object.startsWith("http") && !object.contains("<www.w3.org/2001/XMLSchema#string>") &&  !object.contains("localhost:8890/")){
					if(!uriRepository.containsKey(object)){ // uri doesn't exist yet in the repository.
						uriRepository.put(object, uriCount);
						++uriCount;
						objects.add(uriCount); // a new object uri
					}
					else{
						int uriId = uriRepository.get(object);
						if(subjects.contains(uriId))
							subjectsAndObjects.add(uriId); // the current object uri is also a subject uri for the chosen predicate p_i and endpoint e_n
 						else
							objects.add(uriId); // the current object uri is purely an object uri, which was already visited in some endpoint/predicate before.
					}
				}

// if(objects.size() >= 100000 && objects.size()%100000 == 0)
// System.out.println("Object resources: " + objects.size());
			}

			subjects.removeAll(subjectsAndObjects); // 'subjects' now contains only those uris that are uniquely subject
			if(!subjects.isEmpty() || !objects.isEmpty() || !subjectsAndObjects.isEmpty()){
				ArrayList<HashSet<Integer>> subjObjResources = new ArrayList<>();
				subjObjResources.add(subjectsAndObjects);
				subjObjResources.add(subjects);
				subjObjResources.add(objects);
				resources.put(predicate + ":" + endpoint.second, subjObjResources);
				usedPreds++;
				// System.out.println(usedPreds + ": " + subjObjResources.get(0).size() + ", " + subjObjResources.get(1).size() + ", " + subjObjResources.get(2).size());
				// System.out.println(uriRepository.size());
			}
System.out.print(usedPreds + ", ");

// // System.out.println("Object resources: " + objects.size());
// 			if(subjects.size() !=0 || objects.size() !=0){
// 			Pair<HashSet<String>, HashSet<String>> soPair = new Pair(subjects, objects);
// 			resources.put(predicate + ":" + endpoint.second, soPair);
// 			usedPreds++;
// // HashMap<String, ArrayList<HashSet<Integer>>>
// 			// System.out.print(predicate);
// 			// System.out.print(": # unique SUBS: " + subjects.size());
// 			// System.out.print(";  # unique OBS: " + objects.size());
// 			}
// // System.out.print(usedPreds + ", ");
// System.out.println(usedPreds);
		}

System.out.println();
System.out.println("# Predicates considered for PG: " + resources.size());

		conn.close();
		repo.shutDown();
	}
	
	public void createPredGraphSummary(ArrayList<Pair<String, String>> endpoints, String summaryOutPath){

		createPGedgeLabels();

		ArrayList<MEdge> predicateGraph = new ArrayList<>();
		ArrayList<HashMap<String, ArrayList<HashSet<Integer>>>> endpointResources = new ArrayList<>();
		for(Pair<String, String> endpoint : endpoints){
			System.out.println("EP: " + endpoint.second);
			HashMap<String, ArrayList<HashSet<Integer>>> resources = new HashMap<>();
			try{
				getEndpointResources(endpoint, resources, summaryOutPath);
			}
			catch(Exception RepositoryException){
					System.out.println("Error");
			}
			endpointResources.add(resources);
			System.out.println("# uriRepository: " + uriRepository.size());
		}


	 for(int i = 0; i < endpointResources.size()-1; ++i){
		 for(int j = i + 1; j < endpointResources.size(); ++j){
			 for(Map.Entry<String, ArrayList<HashSet<Integer>>> pred_1 : endpointResources.get(i).entrySet()){
				 for(Map.Entry<String, ArrayList<HashSet<Integer>>> pred_2 : endpointResources.get(j).entrySet()){
					 // updatePredicateGraph();
					 for(int k = 0; k < pred_1.getValue().size(); ++k){
						 if(!Collections.disjoint(pred_1.getValue().get(k), pred_2.getValue().get(0))){
							 MEdge<String, String, ArrayList<Integer>> multiEdge = new MEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+0));
							 predicateGraph.add(multiEdge);
							 if(k == 0)
							 	break; // all possible lables have been considered.
							 else
							 	continue; // all possible labels for the current 'k' have been considered.
						 }
						 else if(!Collections.disjoint(pred_1.getValue().get(k), pred_2.getValue().get(1))){
							 MEdge<String, String, ArrayList<Integer>> multiEdge = new MEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+1));
							 predicateGraph.add(multiEdge);
						 }
						 else if(!Collections.disjoint(pred_1.getValue().get(k), pred_2.getValue().get(2))){
							 MEdge<String, String, ArrayList<Integer>> multiEdge = new MEdge(pred_1.getKey(), pred_2.getKey(), pgEdgeLabels.get(3*k+2));
							 predicateGraph.add(multiEdge);
						 }
					 }
				 }
			 }
			 System.out.println("PREDICATE GRAPH SIZE: " + predicateGraph.size());
	 		}
		}

		try{
			PrintWriter writer = new PrintWriter(summaryOutPath + "predicateGraph.txt", "UTF-8");
			for(int i = 0; i < predicateGraph.size(); ++i){
				writer.println(predicateGraph.get(i).first + " " + predicateGraph.get(i).second + " " + predicateGraph.get(i).mlbl);
			}
			/*
			for(int i = 0; i < predicateGraph.size(); ++i){
				ArrayList<Integer> edgeLabels = predicateGraph.get(i).mlbl;
				String tt = predicateGraph.get(i).first;
				if(edgeLabels.size() == 1)
					writer.println(predicateGraph.get(i).first + " " + predicateGraph.get(i).second + " " + edgeLabels.get(0));
				else{
					writer.print(predicateGraph.get(i).first + " " + predicateGraph.get(i).second + " ");
					for(int j = 0; j < edgeLabels.size(); ++j){
						if(j == edgeLabels.size()-1)
							writer.print(edgeLabels.get(j));
						else
							writer.print(edgeLabels.get(j) + ",");
					}
					writer.println();
				}
			}
			*/
			writer.close();
		}
		catch (IOException i) {
			i.printStackTrace();
		}
	}
	
}
