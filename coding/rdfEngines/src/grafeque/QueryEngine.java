package grafeque;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import grafeque.FileManager;

class RdfSummary{
	static ArrayList<BitSet[][][]> kNeighbours = new ArrayList<>();
	static ArrayList<Integer[][][]> kFrequencies = new ArrayList<>();
}

public class QueryEngine {
	
	public static void main(String [] args) throws IOException{
		
		char operation = args[0].charAt(1);
				
		switch(operation) {
		
			case 't' :{	// (triplestore) generate summaries from local triple stores
				
				String edgeFile = args[1] + args[2]; 
				String summaryPath = args[1]; 		
				int parameter = Integer.parseInt(args[3]);
				
				double startTime = System.currentTimeMillis();
				GraphData rdfGraph = GraphData.initialize();
				FileManager.parseRdfData(edgeFile, rdfGraph);
				System.out.println("Read edges time: " + (System.currentTimeMillis()-startTime)/1000);
				
				startTime = System.currentTimeMillis();
				SummaryBuilder.buildSummary(parameter, rdfGraph, summaryPath);
				System.out.println("Index generation time: " + (System.currentTimeMillis()-startTime)/1000);		
				
				FileManager.printData(summaryPath, (System.currentTimeMillis()-startTime)/1000, (System.currentTimeMillis()-startTime)/1000);
			}
			case 'e' :{	// (endpoint) generate summaries from endpoints
				
				ArrayList<Pair<String, String>> endpoints = new ArrayList<>();
				String epPath = args[1];
				String summaryPath = args[2];
				SummaryBuilder predGraph = new SummaryBuilder();
				try{
					predGraph.readEpFile(epPath, endpoints);
				}
				catch(IOException e){
					e.printStackTrace();
				}
				predGraph.createPredGraphSummary(endpoints, summaryPath);
			}
			case 'q' :{	// (query) perform federated querying
				FileManager.readSPARQLFile(args[1] + args[2]);
				FileManager.readInputEndpoints(args[1] + args[3]);
				GraphData fedSparql;
				
				/*
				for(String ep : FileManager.getEndpoints()) {
					FileManager.readMatrixSummary(args[1] + args[4]);				
					FileManager.readPredicateMapping(args[1] + args[4] + "edge_map.txt"); //store it as a serialized data for faster processing							
				}						
				*/
				
				String sparqlQuery = FileManager.getQuery();
				Set<String> sparqlVars = FileManager.getVariables();
				List<List<String>> sparqlTriples = FileManager.getTriples();
				List<String> sparqlEndpoints = FileManager.getEndpoints();
				Map<String, String> sparqlPrefixes = FileManager.getPrefixes();
					
				QueryManager.parseSparql();
				
			}
		
		}		
		
	}	
}
