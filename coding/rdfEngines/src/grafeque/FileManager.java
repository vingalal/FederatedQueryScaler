package grafeque;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.Queue;

class Pair<X, Y> implements Serializable { 
  public X first; 
  public Y second; 
  public Pair(X first, Y second) { 
    this.first = first; 
    this.second = second; 
  } 
} 

class NodePair{ // Used exclusively for Map implementation (e.g.: edges), in order to have immutable object key for HashMap.
  private final int first; 
  private final int second; 
  public NodePair(Integer first, Integer second) { 
    this.first = first; 
    this.second = second; 
  } 
  
  public int getFirst() {
		return first;
	}
  public int getSecond() {
		return second;
	}
  // Following overrides are necessary to use Pair as a 'key' in a hashmap.
  @Override
  public boolean equals (final Object O) {
    if (!(O instanceof NodePair)) return false;
    if (((NodePair) O).first != first) return false;
    if (((NodePair) O).second != second) return false;
    return true;
  }
  @Override
  public int hashCode() {
    return (first << 16) + second;
  }
  
} 

class GraphData{
  public final ArrayList<Set<Integer>> adjList;
  public final Map<NodePair, ArrayList<Integer>> edges;
  public final Set<Integer> uniqueEdges;
  public final Set<ArrayList<Integer>> uniqueMultiedges;
  
  public GraphData(ArrayList<Set<Integer>> adjList, Map<NodePair, ArrayList<Integer>> edges, Set<Integer> uniqueEdges, Set<ArrayList<Integer>> uniqueMultiedges){
  	this.adjList = adjList;
  	this.edges = edges;
  	this.uniqueEdges = uniqueEdges;
  	this.uniqueMultiedges = uniqueMultiedges; 
  }
  
  public static GraphData initialize(){
	  ArrayList<Set<Integer>> adjList = new ArrayList<Set<Integer>>();
	  Map<NodePair, ArrayList<Integer>> edges = new HashMap<NodePair, ArrayList<Integer>>();   
	  Set<ArrayList<Integer>> uniqueMultiedges = new HashSet<ArrayList<Integer>>();
	  Set<Integer> uniqueEdges = new HashSet<Integer>();
//	  uniqueEdges.to
//	  Collections.so
		GraphData graphData = new GraphData(adjList, edges, uniqueEdges, uniqueMultiedges);
		return graphData;
  }
}



public class FileManager{
	static String query;
	static Set<String> variables = new HashSet<String>(); 
	static List<List<String>> triples = new ArrayList<List<String>>();
	static Map<String, String> prefixes = new HashMap<String, String>();
	static Map<String, Integer> predicateMap = new HashMap<String, Integer>();
	static ArrayList<String> queryEndpoints = new ArrayList<String>();	
	static ArrayList<String> inputEndpoints = new ArrayList<String>();
	static ArrayList<String> endpoints = new ArrayList<String>(); // A complete set of endpoints: SPARQL query endpoints (if any) + input endpoints (if any)

	public static String getQuery(){
		return query;
	}
	public static Set<String> getVariables(){
		return variables;
	}
	public static List<List<String>> getTriples(){
		return triples;
	}
	public static ArrayList<String> getEndpoints(){
		return endpoints;
	}
	public static Map<String, String> getPrefixes(){
		return prefixes;
	}
	public static Map<String, Integer> getPredicateMap(){
		return predicateMap;
	}	

	public static List<String> getPredicates(){
		List<String> predicates = new ArrayList<String>();
		for(int i = 0; i < triples.size(); ++i)
			predicates.add(triples.get(i).get(1));
		return predicates;
	}
	

	public static void readPredicateMapping(String fileName) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try{
			String line = br.readLine();
			while(line != null){
				String[] lineArray = line.split(" ");
				predicateMap.put(lineArray[0], Integer.parseInt(lineArray[1]));
        line = br.readLine();
			}			
		}
    finally {
      br.close();
    }
	}
	
	public static void readInputEndpoints(String fileName) throws IOException{
		if(fileName.contentEquals("-no")) {
			System.out.println("No endpoints provided!");
			return; //No additional endpoints provided
		}
		
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try{
			String line = br.readLine();
			
			while(line != null){
				if(line.trim().equals("")) // an empty line with/without spaces
					line = br.readLine();
				else {
					if(line.charAt(0) != '#')
						inputEndpoints.add(line);
	        line = br.readLine();
				}
			}			
		}
    finally {
      br.close();
    }
		endpoints.addAll(queryEndpoints);
		endpoints.addAll(inputEndpoints);
	}
	
	public static void readSPARQLFile(String fileName) throws IOException{
		BufferedReader br = new BufferedReader(new FileReader(fileName));
	    //boolean varNotListed = true;	    					
		try {
      StringBuilder sb = new StringBuilder();
      String inLine = br.readLine();
      boolean varsDefined = true;
      while (inLine != null) {
        String line = inLine.trim(); 	        	
				if(line.startsWith("#") || line.isEmpty()){
					inLine = br.readLine();
					continue;	        
				}
				List<String> words = Arrays.asList(line.split(" "));
				int wInd = words.indexOf("PREFIX");
				if(wInd != -1){ // collect prefixes (may be of no use)
					prefixes.put(words.get(wInd+1), words.get(wInd+2));
				}
				else if(words.contains("SELECT")){ // collect predefined variables
	    		for(String word : words){
	  		    if(word.startsWith("?")){
	  		    	variables.add(word);
	  		    }        		        
	    		}
	    		if(variables.isEmpty())
	    			varsDefined = false; // No variables defined explicitly            		
				}
				else if(words.contains("SERVICE")){ // collect endpoints for federated querying
					wInd = words.indexOf("SERVICE");
					queryEndpoints.add(words.get(wInd + 1));    				
				}
				else { // collect triples
					if(!varsDefined){ // collect all the variables from the query
						if (words.size() < 3){
							inLine = br.readLine();
							continue;
						}
						if(words.get(0).startsWith("?"))
							variables.add(words.get(0));
						if(words.get(2).startsWith("?"))
							variables.add(words.get(2));    					
						List<String> triple = new ArrayList<String>();
						triple.add(words.get(0)); // subject
						triple.add(words.get(1)); // predicate
						triple.add(words.get(2)); // object
						triples.add(triple);
					}
					else{
						if (words.size() < 3){
							inLine = br.readLine();
							continue;
						}
						List<String> triple = new ArrayList<String>();
						triple.add(words.get(0)); // subject
						triple.add(words.get(1)); // predicate
						triple.add(words.get(2)); // object
						triples.add(triple);    					
					}
				}    			    		
        sb.append(inLine);
        sb.append("\n");
        inLine = br.readLine();
      }
      query = sb.toString();
		}
    finally {
        br.close();
    }
	}
	
	
	@SuppressWarnings("unchecked")
	public static void parseRdfData(String edgeFile, GraphData rdfGraph) throws IOException {  
		BufferedReader br = new BufferedReader(new FileReader(edgeFile));
		try {
      StringBuilder sb = new StringBuilder();
      String inLine = br.readLine();
      // Check the direction of multiedges when rdf is parsed. Use signed integers for predicate values.
//      Map<ArrayList<Integer>, ArrayList<Integer>> edges = new HashMap<ArrayList<Integer>, ArrayList<Integer>>();
      Set<Integer> nodes = new HashSet<Integer>();
      while (inLine != null) {
      	String[] splitLine = inLine.split(" ");
      	Pair<Integer,Integer> nodePair = new Pair<Integer,Integer>(Integer.parseInt(splitLine[0]), Integer.parseInt(splitLine[1]));
      	NodePair nodePairMap = new NodePair(Integer.parseInt(splitLine[0]), Integer.parseInt(splitLine[1]));
      	nodes.add(nodePair.first);
      	nodes.add(nodePair.second);
      	String[] stringPredicates = splitLine[2].split(",");
      	/// When multiedges are considered
      	ArrayList<Integer> predicates = new ArrayList<Integer>(stringPredicates.length);
      	
      	for(int i = 0; i < stringPredicates.length; ++i){
      		int edge = Integer.parseInt(stringPredicates[i]);
      		predicates.add(i, edge);
      		rdfGraph.uniqueEdges.add(edge);
      	}
      	Queue<Integer> q = new LinkedList<>();

      	rdfGraph.edges.put(nodePairMap, predicates);
      	rdfGraph.uniqueMultiedges.add(predicates);
      	/// When simple edges are considered
//      	for(int i = 0; i < stringPredicates.length; ++i){
//      		int edge = Integer.parseInt(stringPredicates[i]);
//      		rdfGraph.edges.put(nodePairMap, edge);
//      		rdfGraph.uniqueEdges.add(edge);
//      	}

      	inLine = br.readLine();
    	}
      GraphOperations.getAdjList(rdfGraph, nodes.size());
      		}
    finally {
      br.close();
    }
	}
	
	
public static void printMatrixSummary(Map<Integer, ArrayList<Map<Integer, Pair<BitSet, MutableInt>>>> matrixSummary, int nEdges, int K, String summaryPath) {
	
	Integer[][][] freqArray  = new Integer[nEdges][nEdges][K];
	for(int i = 0; i < nEdges; ++i)
		for(int j = 0; j < nEdges; ++j)
			for(int k = 0; k < K; ++k)
				freqArray[i][j][k] = 0;
	BitSet[][][] dirArray = new BitSet[nEdges][nEdges][K];
//	dirArray[0][0][0].a
	
	for(Map.Entry<Integer, ArrayList<Map<Integer, Pair<BitSet, MutableInt>>>> summary : matrixSummary.entrySet()) {
		for(int k = 0; k < K; ++k) {
			for(Map.Entry<Integer, Pair<BitSet, MutableInt>> sA : summary.getValue().get(k).entrySet()) {
				freqArray[summary.getKey()][sA.getKey()][k] = sA.getValue().second.get();
				dirArray[summary.getKey()][sA.getKey()][k] = sA.getValue().first;
			}				
		}
	}
	
	// Print to a text file
	try{
    PrintWriter fileOut = new PrintWriter(summaryPath + "frequency.txt", "UTF-8");
		for(int i = 0; i < nEdges; ++i){
			for(int j = 0; j < nEdges; ++j){
				for(int k = 0; k < K; ++k) {
					if(k == K-1)
						fileOut.print(String.valueOf(freqArray[i][j][k]) + " ");
					else
						fileOut.print(String.valueOf(freqArray[i][j][k]) + ",");					
				}
			}
			fileOut.println();
		}
		fileOut.close();
	}
  catch (IOException i) {
    i.printStackTrace();
  }
	
	try{
    PrintWriter fileOut = new PrintWriter(summaryPath + "kNeighbors.txt", "UTF-8");
		for(int i = 0; i < nEdges; ++i){
			for(int j = 0; j < nEdges; ++j){
				for(int k = 0; k < K; ++k)
					fileOut.print(String.valueOf(dirArray[i][j][k]));
				fileOut.print(" ");
			}
			fileOut.println();
		}
		fileOut.close();
	}
  catch (IOException i) {
    i.printStackTrace();
  }
	
	// Print serialized data to a binary file
  try {
    FileOutputStream fileOut = new FileOutputStream(summaryPath + "kNeighbors.ser");
    ObjectOutputStream out = new ObjectOutputStream(fileOut);
    out.writeObject(dirArray);
    out.close();
    fileOut.close();
  }
  catch (IOException i) {
    i.printStackTrace();
  }
  
	/// Write to a binary
  try {
    FileOutputStream fileOut = new FileOutputStream(summaryPath + "frequency.ser");
    ObjectOutputStream out = new ObjectOutputStream(fileOut);
    out.writeObject(freqArray);
    out.close();
    fileOut.close();
  }
  catch (IOException i) {
    i.printStackTrace();
  }	
  System.out.println("Serialized data is saved");
}


public static void readMatrixSummary(String summaryPath){
  /// Read from binary
	BitSet[][][] kNeighbors; // = new BitSet[nEdges][nEdges][K];
  try {
    FileInputStream fileIn = new FileInputStream(summaryPath + "kNeighbors.ser");
    ObjectInputStream in = new ObjectInputStream(fileIn);
    kNeighbors =  (BitSet[][][]) in.readObject();
    in.close();
    fileIn.close();
    System.out.println("Serialized data is read");
  } 
  catch (IOException | ClassNotFoundException i) {
    i.printStackTrace();
    return;
  }
  
	Integer[][][] frequency; // = new Integer[nEdges][nEdges][K];
  try {
    FileInputStream fileIn = new FileInputStream(summaryPath + "frequency.ser");
    ObjectInputStream in = new ObjectInputStream(fileIn);
    frequency =  (Integer[][][]) in.readObject();
    in.close();
    fileIn.close();
    System.out.println("Serialized data is read");
  } 
  catch (IOException | ClassNotFoundException i) {
    i.printStackTrace();
    return;
  }
  
  RdfSummary.kNeighbours.add(kNeighbors);
  RdfSummary.kFrequencies.add(frequency);
  
  // Print the serialized values back
  int nEdges = frequency.length;
  int K = frequency[0][0].length;
	for(int i = 0; i < nEdges; ++i){
		for(int j = 0; j < nEdges; ++j){
			for(int k = 0; k < K; ++k)
				System.out.print(String.valueOf(kNeighbors[i][j][k]) + ",");
			System.out.print(" ");
		}
		System.out.println();
	}
	for(int i = 0; i < nEdges; ++i){
		for(int j = 0; j < nEdges; ++j){
			for(int k = 0; k < K; ++k)
				System.out.print(String.valueOf(frequency[i][j][k]) + ",");
			System.out.print(" ");
		}
		System.out.println();
	}		
}


	public static void printRdfSummary(Map<Integer, ArrayList<Set<Integer>>> rdfSummary, int nEdges, int K, String summaryPath){
		Integer[][][] intArray  = new Integer[nEdges][nEdges][K]; // RDF summary in integer matrix
		
		for(int i = 0; i < nEdges; ++i)
			for(int j = 0; j < nEdges; ++j)
				for(int k = 0; k < K; ++k)
					intArray[i][j][k] = 0;
			
		for(Map.Entry<Integer, ArrayList<Set<Integer>>> summary : rdfSummary.entrySet()){
			for(int k = 0; k < summary.getValue().size(); ++k){
				Set<Integer> predicateSet = summary.getValue().get(k);
				for(Integer pred : predicateSet){
					if(pred < nEdges){
						if(predicateSet.contains(pred + nEdges))
							intArray[summary.getKey()][pred][k] = 3;
						else
							intArray[summary.getKey()][pred][k] = 1;
					}
					else{
						if(predicateSet.contains(pred - nEdges))
							intArray[summary.getKey()][pred - nEdges][k] = 3;
						else
							intArray[summary.getKey()][pred - nEdges][k] = 2;
					}
				}
			}
		}
		
		BitSet[][] bitArray = new BitSet[nEdges][nEdges];  // RDF summary in bit matrix
		for(int i = 0; i < nEdges; ++i){
			for(int j = 0; j < nEdges; ++j){
				BitSet bitVector = new BitSet(2*K);
				for(int k = 0; k < K; ++k){
					if(intArray[i][j][k] == 3){
						bitVector.set(2*k);
						bitVector.set(2*k+1);
					}
					else if(intArray[i][j][k] == 2)
						bitVector.set(2*k);
					else if(intArray[i][j][k] == 1)
						bitVector.set(2*k+1);
				}
				bitArray[i][j] = bitVector;
			}
		}
		
//		for(int i = 0; i < nEdges; ++i){
//			for(int j = 0; j < nEdges; ++j)	
//				System.out.print(bitArray[i][j] + ",\t");
//			System.out.println();
//		}
		
	try{
    PrintWriter fileOut = new PrintWriter(summaryPath + "indexBitTest.txt", "UTF-8");
		for(int i = 0; i < nEdges; ++i){
			for(int j = 0; j < nEdges; ++j){
				for(int k = 0; k < K; ++k)
					fileOut.print(String.valueOf(intArray[i][j][k]));
				fileOut.print(" ");
			}
			fileOut.println();
		}
		fileOut.close();
	}
  catch (IOException i) {
    i.printStackTrace();
  }
			
		/// Write to a binary
    try {
    	System.out.println("size: " +  intArray.length);
      FileOutputStream fileOut = new FileOutputStream(summaryPath + "indexBitTest.ser");
      ObjectOutputStream out = new ObjectOutputStream(fileOut);
      out.writeObject(bitArray);
      out.close();
      fileOut.close();
      System.out.println("Serialized data is saved");
    }
    catch (IOException i) {
      i.printStackTrace();
    }
	}
	
	public static void printPredicateTrie(Trie predicateTrie, String summaryPath){
	  try {
	  	System.out.println("Printing to a file...");
	    FileOutputStream fileOut = new FileOutputStream(summaryPath + "indexTrieTest.ser");
	    ObjectOutputStream out = new ObjectOutputStream(fileOut);
	    out.writeObject(predicateTrie);
	    out.close();
	    fileOut.close();
	    System.out.println("Trie index has been successfully stored");
	  }
	  catch (IOException i) {
	    i.printStackTrace();
	  }
	}
  
	public static void readRdfSummary(String file, int nEdges){
	  /// Read from binary
	  BitSet[][] bitArrayNew;
	  try {
	    FileInputStream fileIn = new FileInputStream("/home/vijay/Work/Data/RDF/triplestores/FedBench/DrugBank/indexBitTest.ser");
	    ObjectInputStream in = new ObjectInputStream(fileIn);
	    bitArrayNew =  (BitSet[][]) in.readObject();
	    in.close();
	    fileIn.close();
	    System.out.println("Serialized data is read");
	  } 
	  catch (IOException | ClassNotFoundException i) {
	    i.printStackTrace();
	    return;
	  }
	  
//		for(int i = 0; i < nEdges; ++i){
//			for(int j = 0; j < nEdges; ++j)	
//				System.out.print(bitArrayNew[i][j] + ",\t");
//			System.out.println();
//		}		
	}

	public static void printData(String summaryPath, double readTime, double indexTime) {
		try{
	    PrintWriter fileOut = new PrintWriter(summaryPath + "log.txt", "UTF-8");
	    fileOut.println("Index generation time: " + indexTime);
	    fileOut.println("File read time: " + readTime);
			fileOut.close();
		}
	  catch (IOException i) {
	    i.printStackTrace();
	  }
	}
	
}


