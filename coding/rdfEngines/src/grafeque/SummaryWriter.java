package grafeque;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class SummaryWriter{

  public static void writePredicateMappings(String summaryOutPath, List<String> predicates){
    try{
      PrintWriter writer = new PrintWriter(summaryOutPath, "UTF-8");
      int m = 0;
      for(String predicate : predicates){
      	if (!predicate.contains("openlinksw.com")){
      		writer.println("<" + predicate + ">" + " " + m);
          ++m;
        }
      }
      writer.close();
    }
    catch (IOException i) {
      i.printStackTrace();
    }
  }
}
