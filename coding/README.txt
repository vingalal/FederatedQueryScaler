1. Building Grafeque Indexes:
	cd fedx-grafeque
	java -Xmx15360m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -e ".../indexes/endpoints.txt" @s ".../indexes/"

	- "./indexes/endpoints.txt" is the path to a file that has a list of endpoints for which predicate graph has to be built
	- "./indexes/" is the path where indexes are outputted

2. Executing Grafeque API
	cd fedx-grafeque
	java -Xmx4096m -cp bin:lib/* fr.inria.dyliss.grafeque.CLI -d ".../indexes/endpoints.ttl"  -f "STDOUT" @s ".../indexes/" @q ".../queries/ls2"

	- "./indexes/endpoints.ttl" has the list of endpoints queried by Grafeque
	- "./indexes/" has the index structures required by Grafeque
	- "./queries/" query folder
	- "./results" results folder with formats [XML | JSON] by setting the argument -f;

